\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,enumitem}
\usepackage[pdftex]{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{movie15}
\usepackage{listings}
\usepackage[version=4]{mhchem}
\usepackage[font=footnotesize,format=plain,labelfont=bf]{caption}
\usepackage{tcolorbox}
\tcbuselibrary{theorems}
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\newtcbtheorem[number within=section]{mytest}{Test}%
{colback=brown!50!white,colframe=brown!80!black,fonttitle=\bfseries}{th}

\newtcbtheorem[number within=section]{mydependencies}{Dependencies}%
{colback=brown!50!white,colframe=brown!80!black,fonttitle=\bfseries}{th}


\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\input{../xpacc-macros-paper}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\pagestyle{empty}

\begin{center}
\vspace*{1.5in}

\textsc{\color{myBrown} \huge XPACC:  A guide to using tools}\\
\bigskip
{\color{myTan} The Center for Exascale Simulation of Plasma-coupled Combustion} \\
{\color{myTan} \textit{University of Illinois at Urbana--Champaign}} \\

\vfil
\begin{minipage}{0.8\textwidth}
This document describes using tools on workhorse platforms (e.g. Quartz and Lassen) with direct impact on Y5 simulations.  The document current covers \textit{Hydra} and \textit{Leap}.

\end{minipage}

\vfil
\end{center}


\newpage
\setcounter{page}{1}
\pagestyle{plain}


\newpage

\section{Hydra for Y5}
Given you have the dependencies for running \textit{Hydra} in \ref{th:hydra}, the \texttt{cmake} on Lassen shown below this.
\begin{lstlisting}[language=Python]
CC=mpicc CXX=mpicxx FC=mpif90 cmake ..  -DCMAKE_PREFIX_PATH=/usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/Install -DCMAKE_BUILD_TYPE=Release  -DAUTODETECT_MPI=ON -DBUILD_STATIC=YES  -DBUILD_DOCUMENTATION=off -DBUILD_PROVENANCE=NO -DDISABLE_WARNINGS=YES  -DENABLE_CANTERA=1  -DUSE_HYDRA=1 
\end{lstlisting}

\begin{mydependencies}{Dependencies for hydra}{hydra}
\begin{itemize}
\item CUDA
\item OpenMP
\end{itemize}
\end{mydependencies}

Hybrid execution relies on setting environment variables \texttt{HYB\_GPUS=g} and \texttt{HYB\_CPU=c} where $g$ and $c$ represent the number of GPUs and CPU threads used in the execution, respectively.  On Lassen, setting the environment is done through \texttt{jsrun} shown below this.

\pagebreak
\subsection{Running on Lassen}
Lassen uses \texttt{jsrun}, which is part of IBM's job step manager (JSM), to replace calls like mpiexec.  Arguments for \texttt{jsrun} provide sufficient granularity for determining how to allocate the job per node.    Here, I provide an abbreviated list of commands, which I think is adequate for running our jobs though further tuning may be necessary.

\begin{center}
\begin{tabular}{l l } 
 \hline
\texttt{-n} & Total number of resource sets \\ 
\texttt{-r} &  Resource sets per node\\ 
\texttt{-a} & Tasks (ranks) per resource set \\ 
\texttt{-c} & CPUS per resource set \\ 
\texttt{-g} & GPUs per resource set\\
\texttt{-E} & Export environment variables (e.g. \texttt{OMP\_NUM\_THREADS})\\
 \hline
\end{tabular}
\end{center}

The Power9 architecture has 2 sockets; each socket has 20 cores and 2 Tesla V100.  Each core has 4 threads.  Based on the architecture and resource contention, runs should use 2 resource sets per node \texttt{-r 2} and can specify one task per resource set, \texttt{-a 1}. Based on this, I think you could specify \texttt{-c 20} giving you access to the 80 threads per resource set.  Based on the table below, this indeed gives the best performance.  For more than one node, the total number of resource sets will need included; that is \texttt{-n} should be equal to r$\cdot$N nodes.  An example command for 1 node will look like
\begin{lstlisting}[language=Python]
jsrun -r 2 -a 1 -c 20 -E HYB_GPUS=0 -E HYB_CPU=80 ./plascom2x -p 2 -c curvilinear.config
\end{lstlisting}

I have tested the following on 4th order SBP AcousticPulsePeriodi3D with $N_x=N_y=N_z=160$.  The following table reports the time of the right hand side for a range of jsrun inputs.

\begin{center}
\begin{table}[h!]
\caption {($^\star$) indicates a \texttt{-DUSE\_HYDRA=0}. `crash' indicates `Missing Target for offloading after successful target offloading.  The program will stop.'} \label{tab:title} 
\centering\begin{tabular}{c c c c c c } 
 \hline
\texttt{-r} & \texttt{-a} & \texttt{-c}  & \texttt{-E HYB\_GPUS} &\texttt{-E HYB\_CPU} & RHS Time (s) \\ 
 \hline
2 & 1 & 1 & 0 &10 & 14.3 \\ 
2 & 1 & 1 & 0 &20 & 9.2 \\
2 & 1 & 1 & 0 &40 & 6.3 \\ 
2 & 1 & 1 & 0 &80 & 4.9 \\
2 & 1 & 20 &0 & 4 & 27.0 \\
2 & 1 & 20 &0 & 80 & 3.9 \\
2 & 5 & 5 & 0 & 1 & 24.5\\
2 & 5 & 5 & 0 & 4 & 7.8\\
$2$ & 10 & 10 & 0 & 1 & \textcolor{red}{crash}\\
$2$ & 20 & 20 & 0 & 1 &\textcolor{red}{crash}\\
$2^\star$ & 10 & 10 & 0 & 1 & 2.8\\
$2^\star$ & 20 & 20 & 0 & 1 &1.5\\
 \hline
\end{tabular}
\end{table}
\end{center}

\textcolor{red}{Do send along simpler set of commands if you identify it and quantify its relative performance.}

\pagebreak
\subsection{Running OpenMP on Quartz}
To guide decomposition and runtime settings on Lassen, we consider OpenMP enabled version of Plascom2 running on Quartz for the 10-mesh Y5 case, which represents the discretization for the patched laser-induced breakdown simulations.  The list of meshes and size are given below
\begin{center}
\begin{tabular}{l l c c } 
Grid & Description & $(N_x, N_y, N_z)$ & Total points ($\times 10^6$) \\ 
 \hline
1 & Top boundary layer & (1024,13,64) & 0.85 \\ 
2 &  Bottom boundary layer & (512,13,64)& 0.43\\ 
3 & Cavity & (359,124,78) & 3.5\\ 
4 & Bottom combustor& (401,13,64)&0.33 \\ 
5 & Wedge& (37,124,78)&0.36\\
6 & Core tunnel& (1024,56,64)&3.7\\
7 & Cylindrical injector with collar& (50,45,33)&0.07\\
8 & Rectangular injector axis& (50,20,20) & 0.02\\
9 & Innermost LIB& (360,180,180)&11.6\\
10 & Intermediate LIB to cavity& (150,300,150)&6.75\\
 \hline
Total & & & 27.6\\
\end{tabular}
\end{center}

For a rank-based approach, consider 20 points per direction the total number of ranks applying the ceiling function $(C_x,C_y,C_z)=(\ceil{N_x/20},\ceil{N_y/20},\ceil{N_z/20})$ is shown in table \ref{tableCeiling}. Rounding up, rather than rounding down is used for cases in which $N_i < N_{\mathrm{PPD}}$ which would used 0 cores in that direction, which is not physical.  However, for $N_i \ge N_{\mathrm{PPD}}$, then rounding resources down.

\begin{center}
\begin{tabular}{l c c c c } 
Grid & $(N_x, N_y, N_z)$ & $(C_x, C_y, C_z)$ & Total ranks & Points per rank \\ 
 \hline
1 & (1024,13,64) & (52,1,4) & 208 & 4096  \\ 
2 &  (512,13,64) & (26,1,4) & 104 & 4096\\ 
3 &  (359,124,78) & (18,7,4) & 504 & 6889\\ 
4 & (401,13,64) & (21,1,4) & 84 & 3971 \\ 
5 &  (37,124,78) & (2,7,4) & 56 & 6390\\
6 & (1024,56,64) & (52,3,4) & 624 & 5881\\
7 &  (50,45,33) & (3,3,2) & 18 & 4125\\
8 & (50,20,20) & (3,1,1) & 3 & 6666\\
9 & (360,180,180) & (18,9,9) & 1458 & 8000\\
10 & (150,300,150) & (8,15,8) & 960 & 7031\\
 \hline
Total &  &  & 4019 &\\
\end{tabular}
\end{center}
which gives an imbalance due to the rounding procedure and the meshes sizes.  For meshes (1,2,4) in particular, the 13 points in $N_y$ is coarse enough that rounding causes the points per rank to be smaller than the remaining meshes.  Mesh 9 will take 2x longer to run than mesh 4 if time per time step is the same among the meshes.  Meshes were designed toward $2^n$, for example $N_x=1024$.  However, for overset meshes and extending fringes, the mesh sizes do not follow this paradigm.  In hindsight, the smallest boundary layer mesh (13 pts) should have been made larger but this was chosen based on finite difference stencil fitting between two boundaries, which needs $\ge 12 $ points.

Considering a thread-based approach, we target $N_{\mathrm{PPT}}$ and each grid must 

\subsection{A script to evaluate performance}
A basic example the scale-up a problem used to generate performance data similar to that in Diener et al 2018 is given below this.  

\begin{lstlisting}[language=Python]
#!/bin/bash

function setOption() {
    if grep -q "$2" $1
    then
     sed -i "s/^.*$2.*$/$2 $3/g" $1
    fi
}

CPU=0
GPU=1
rm timing-c${CPU}-g${GPU}.txt
for i in 10 20 40 80 160
do
 echo $i
 timing=PlasCom2Timing_000001.txt
 if [ -f "${timing}" ]; then rm $timing; fi
 setOption box3d.config "box:grid3:NumPoints =" "${i} ${i} ${i}" #setting the box size to be based on loop variable
 setOption curvilinear.config "PlasCom2:NumSteps =" '2'
 setOption curvilinear.config "PlasCom2:NumStepsIO =" '-1'   #disable i/o
 setOption acousticPulse3d.curvilinear.config  "Param:Flags          =" '32' #WENO
 setOption acousticPulse3d.curvilinear.config "SpatialOrder = " '4' #NEEDED FOR WENO
 jsrun -n 1 -a 1 -c 1 -g 0 -E OMP_NUM_THREADS=1 -E HYB_GPUS=${GPU} -E HYB_CPU=${CPU} ./plascom2x -p 2 -c curvilinear.config
 rhs=$(grep -nri 'plascom2x:RHS ' $timing)
 value=$(echo $rhs | cut -d" " -f2)
 echo "${i} ${value}" >> timing-c$CPU-g$GPU.txt

done
\end{lstlisting}

\subsection{Performance expectation}
\textcolor{red}{None at this time.}

\subsection{Correctness}
Several tests are used to evaluate correctness.  The \texttt{Hydra} enabled or disabled should minimally reproduce same answer using 1CPU.  It is anticipated that offloaded code using cuda and OpenMP would produce an answer a different answer due to machine precision. The first ensures we observe correct typical values of flow variables for flows like an acoustic pulse. 

Running
\begin{lstlisting}[language=Python]
[buchta2@lassen580:PlasCom2]$ jsrun -n 1 -a 1 -c 1 -g 0 -E OMP_NUM_THREADS=1 -E HYB_GPUS=1 -E HYB_CPU=0 ./plascom2x -p 2 -c curvilinear.config
\end{lstlisting}
yields
\begin{lstlisting}[language=Python]
...
...
plascom2x: Computing DV before stepping.
plascom2x:  Step = 0 Time = 0
plascom2x: Domain Wide : rho(1,1)	pressure(1,1.01)	temperature(1,1.01)	
plascom2x: 
plascom2x: Computing DV before I/O
plascom2x: Performing I/O at step 0
plascom2x: I/O done.
plascom2x:  Step = 1 Time = 0.001
plascom2x: Domain Wide : rho(1.79769e+308,2.22507e-308)	pressure(1.79769e+308,2.22507e-308)	temperature(1.79769e+308,2.22507e-308)
\end{lstlisting}	

where running on 1 cpu thread 

\begin{lstlisting}[language=Python]
[buchta2@lassen580:PlasCom2]$ jsrun -n 1 -a 1 -c 1 -g 0 -E OMP_NUM_THREADS=1 -E HYB_GPUS=0 -E HYB_CPU=1 ./plascom2x -p 2 -c curvilinear.config
\end{lstlisting}

yields

\begin{lstlisting}[language=Python]
...
...
plascom2x: Computing DV before stepping.
plascom2x:  Step = 0 Time = 0
plascom2x: Domain Wide : rho(1,1)	pressure(1,1.01)	temperature(1,1.01)	
plascom2x: 
plascom2x: Computing DV before I/O
plascom2x: Performing I/O at step 0
plascom2x: I/O done.
plascom2x:  Step = 1 Time = 0.001
plascom2x: Domain Wide : rho(0.999997,1)	pressure(1,1.01)	temperature(1,1.01)
\end{lstlisting}	

yet with 8 threads
\begin{lstlisting}[language=Python]
jsrun -n 1 -a 1 -c 1 -g 0 -E OMP_NUM_THREADS=1 -E HYB_GPUS=0 -E HYB_CPU=8 ./plascom2x -p 2 -c curvilinear.config
\end{lstlisting}	
the solution fails before time stepping.

\begin{lstlisting}[language=Python]
plascom2x: Computing DV before stepping.
plascom2x:  Step = 0 Time = 0
plascom2x: Domain Wide : rho(1,1)	pressure(0,1.01)	temperature(0,1.01)	
plascom2x: 
plascom2x: Computing DV before I/O
plascom2x: Performing I/O at step 0
plascom2x: I/O done.
\end{lstlisting}	

%AcousticPulsePeriodic3d (using WENO) shows similar behavore to two dimensional cases and TORO1 and to Cantera enabled cases C2H4O2.

For the C2H4O2 case, 1CPU v. 1 GPU gives different results after 5 steps
\begin{lstlisting}[language=Python]
Failed rhoV-1 check:      
	 absolute error:     20.1367
	 relative error:     0.325221
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed rhoV-2 check:      
	 absolute error:     2.43393
	 relative error:     1
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed rhoE check: 
	 absolute error:     7627.66
	 relative error:     0.033908
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed scalarVar-4 check: 
	 absolute error:     0.0473799
	 relative error:     0.0353886
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed scalarVar-15 check: 
	 absolute error:     0.00408816
	 relative error:     0.0326327
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed pressure check: 
	 absolute error:     5043.57
	 relative error:     0.0480141
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
Failed temperature check: 
	 absolute error:     3.95215
	 relative error:     0.0124765
	 zero tolerance:     1e-16
	 absolute tolerance: 2.22507e-308
	 relative tolerance: 1e-15
\end{lstlisting}	






\section{Leap for Y5}
Based off of Macart and Mikida work and slides in google slide deck.
\begin{mydependencies}{Dependencies for \textit{Leap}}{hydra}
\begin{itemize}
\item Illinois computing environment (ICE)
\item Spack (recommended)
\item xSpack (recommended)
\item Python
\item C++ 11
\end{itemize}
\end{mydependencies}




\subsection{Performance expectation}
Given the specified time discretization on the slow `s' and fast `f' grids. The cost of all right hand side evaluations between the schemes are given
\begin{equation}
C_{RK4}=4\left(N_{p,s} + N_{p,f}\right)\frac{T}{\Delta t_{RK4}} \times C_{RHS}
\end{equation}
and for Adams--Bashforth
\begin{equation}
C_{AB}=\left(N_{p,s} + S_r N_{p,f}\right)\frac{T}{\Delta t_{AB}} \times C_{RHS,AB}
\end{equation}
\noindent where $T$ is time interval of interest, $\Delta t$ are the stable time steps for the respective schemes, $N_{p}$ is the number of points on the slow and fast grid respectively, $C_{RHS}$ the cost of right-hand-side evaluation, and $S_r$ is the step ratio.  The Adams-Bashforth requires less right-hand-side evaluations but its time step is more restrictive $\Delta t_{AB} \lesssim \frac{1}{3} \Delta t_{RK4}$. The cost of right-hand side for RK4 and Adams-Bashforth are approximately the same although the Adams-Bashforth requiring a linear solve to evalue a 2x2 matrix.
\subsection{Script to evaluate performance}
\subsection{Script to modify decompositions based on fast and slow grids}
The choice of domain decomposition is key to performance.  Thus, requiring modification for the amount of computer resources attached to each slow and fast grids.  As such, the work should be distribution depending approximately on the inverse of the step ratio $S_r$.


%    <!-- u'    species O2    ' -->
%    <species name="O2">
%      <atomArray>O:2 </atomArray>
%      <note>u'TPIS89'</note>
%      <thermo>
%        <NASA Tmax="1000.0" Tmin="200.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             3.782456360E+00,  -2.996734160E-03,   9.847302010E-06,  -9.681295090E-09,
%             3.243728370E-12,  -1.063943560E+03,   3.657675730E+00</floatArray>
%        </NASA>
%        <NASA Tmax="3500.0" Tmin="1000.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             3.282537840E+00,   1.483087540E-03,  -7.579666690E-07,   2.094705550E-10,
%             -2.167177940E-14,  -1.088457720E+03,   5.453231290E+00</floatArray>
%        </NASA>
%      </thermo>
%    </species>
%
%    <!-- u'    species O2    ' -->
%    <species name="O2">
%      <atomArray>O:2 </atomArray>
%      <note>u'TPIS89'</note>
%      <thermo>
%         <NASA9 P0="100000.0" Tmax="250.0" Tmin="100"><floatArray name="coeffs" size="9">
%                 0.0000000000000000E+00,   0.0000000000000000E+00,   3.5607105190581194E+00,  -2.0466870914503943E-04,
%                 0.0000000000000000E+00,   0.0000000000000000E+00,   0.0000000000000000E+00,  -8.5308490068563003E+00,
%                 5.3362050481304877E+00</floatArray></NASA9><NASA9 Tmax="310.00" Tmin="250.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 4.7584176485728538E+08,  -1.0259801865153080E+07,   9.2077224830272637E+04,  -4.4021331254825202E+02,
%                 1.1826211490968490E+00,  -1.6926234451008701E-03,   1.0083255255490369E-06,   4.4587225995681792E+07,
%                -4.6468085486383509E+05</floatArray>
%         </NASA9>
%         <NASA9 Tmax="370.00" Tmin="310.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 2.0357508993954849E+09,  -3.6057616543088332E+07,   2.6591806891255907E+05,  -1.0451296953257199E+03,
%                 2.3088995148599110E+00,  -2.7184596548386550E-03,   1.3326475689456591E-06,   1.6378422280212241E+08,
%                -1.3941896065724781E+06</floatArray>
%         </NASA9>
%         <NASA9 Tmax="430.00" Tmin="370.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 4.6079388835324459E+09,  -6.9272705254907653E+07,   4.3369288673640811E+05,  -1.4473276311050620E+03,
%                 2.7155016955943632E+00,  -2.7158405209977842E-03,   1.1311501353445060E-06,   3.2600934939546430E+08,
%                -2.3448214102417491E+06</floatArray>
%         </NASA9>
%         <NASA9 Tmax="600.00" Tmin="430.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 4.5736801048306106E+06,  -5.3910530362204023E+04,   2.6820016887381479E+02,  -6.9382143479080660E-01,
%                 1.0236407895866080E-03,  -8.0198406203113175E-07,   2.6077661339087839E-10,   2.6683157683213789E+05,
%                -1.4897060315885301E+03</floatArray>
%         </NASA9>
%         <NASA9 Tmax="660.00" Tmin="600.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 8.2072982876098770E+10,  -7.8267064308683991E+08,   3.1092503033705880E+06,  -6.5862584484333865E+03,
%                 7.8460970812128439E+00,  -4.9839760948235034E-03,   1.3188507455346550E-06,   4.0395069119493041E+09,
%                -1.8224515060900521E+07</floatArray>
%         </NASA9>
%         <NASA9 Tmax="720.00" Tmin="660.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 1.8903518688849939E+11,  -1.6454328357013791E+09,   5.9666489023785824E+06,  -1.1537266018126080E+04,
%                 1.2546485495369840E+01,  -7.2755176355017940E-03,   1.7575932997459320E-06,   8.6425431408488350E+09,
%                -3.5517160949024826E+07</floatArray>
%         </NASA9>
%         <NASA9 Tmax="780.00" Tmin="720.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 3.6532692353804523E+11,  -2.9249703375974832E+09,   9.7563122259130795E+06,  -1.7353373212218881E+04,
%                 1.7359629837504340E+01,  -9.2604342728240634E-03,   2.0580060025565671E-06,   1.5607667093537251E+10,
%                -5.8890606479255557E+07</floatArray>
%         </NASA9>
%         <NASA9 Tmax="840.00" Tmin="780.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 6.3099807119601331E+11,  -4.6771692796585484E+09,   1.4443476640650280E+07,  -2.3785029052671009E+04,
%                 2.2029421602284639E+01,  -1.0880429512482170E-02,   2.2388374778342800E-06,   2.5317982708872250E+10,
%                -8.8296220437048197E+07</floatArray>
%         </NASA9>
%         <NASA9 Tmax="1010.00" Tmin="840.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 9.3432733003516346E+07,  -5.9153580865712150E+05,   1.5605675838666241E+03,  -2.1816843117927891E+00,
%                 1.7183382652886450E-03,  -7.2069665480850086E-07,   1.2574041501797030E-10,   3.2958995431032730E+06,
%                -9.7633602274488603E+03</floatArray>
%         </NASA9>
%         <NASA9 Tmax="2900.00" Tmin="1010.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 5.1962771926786093E+05,  -2.7161768622057580E+03,   8.3447107978071262E+00,  -3.0523337568321460E-03,
%                 1.3079165966541270E-06,  -2.6187216875058969E-10,   2.0310220811808240E-14,   1.5850177125852941E+04,
%                -2.7453107444515300E+01</floatArray>
%         </NASA9>
%         <NASA9 Tmax="5730.00" Tmin="2900.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                -2.3216463104939088E+06,   5.2715880032406367E+03,  -6.2172143949771930E-01,   2.2083097452689172E-03,
%                -4.1238393679315882E-07,   3.7062515618334281E-11,  -1.3078182686572660E-15,  -3.5343587928728732E+04,
%                 3.6546957129066968E+01</floatArray>
%         </NASA9>
%         <NASA9 Tmax="12140.00" Tmin="5730.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 1.8603047703356009E+07,  -2.1183692681336990E+04,   1.2860347253348580E+01,  -1.3922394153373420E-03,
%                 1.2385991412826289E-07,  -5.3945867184523437E-12,   9.2804975658158144E-17,   1.5517773998237521E+05,
%                -7.0305074716809827E+01</floatArray>
%         </NASA9>
%         <NASA9 Tmax="26490.00" Tmin="12140.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                -3.3417546330784161E+07,   2.4802130911844692E+04,  -5.9695446901660021E-01,   5.2569780128811430E-04,
%                -2.3272016736499370E-08,   4.8879948995371881E-13,  -4.0473157503074744E-18,  -1.9862195422619209E+05,
%                 4.4437840049752268E+01</floatArray>
%         </NASA9>
%         <NASA9 Tmax="57630.00" Tmin="26490.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                -4.4736695158198193E+07,  -1.6244459483547869E+04,   7.5062401849273792E+00,  -1.1092867329211400E-04,
%                 1.9819322980904431E-09,  -1.7971672317919231E-14,   6.6456893689229253E-20,   1.2287856661615390E+05,
%                -2.9009640537697980E+01</floatArray>
%         </NASA9>
%         <NASA9 Tmax="201100.00" Tmin="57630.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                -2.5600306226476970E+08,   3.5735478812498412E+04,   3.9294300017750290E+00,   5.1565421961530917E-06,
%                -2.6908958887809100E-11,   7.5587518026196049E-17,  -8.8468046001056281E-23,  -3.5036333576997998E+05,
%                 6.7467753308841409E+00</floatArray>
%         </NASA9>
%         <NASA9 Tmax="275000.00" Tmin="201100.00" P0="100000.0">
%           <floatArray name="coeffs" size="9">
%                 4.9976893428852969E+08,   5.0594136719857752E+03,   4.4608601587295622E+00,   1.7406286918916720E-07,
%                -4.5532742021528095E-13,   6.5427898935288563E-19,  -3.9995645935701389E-25,  -2.4992762613681149E+04,
%                 7.4818481392192382E-01</floatArray>
%         </NASA9>
%      </thermo>
%    </species>
%
%
%
%
%    <!-- u'    species C2H4    ' -->
%    <species name="C2H4">
%      <atomArray>H:4 C:2 </atomArray>
%      <note>u'L1/91'</note>
%      <thermo>
%        <NASA Tmax="1000.0" Tmin="200.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             3.959201480E+00,  -7.570522470E-03,   5.709902920E-05,  -6.915887530E-08,
%             2.698843730E-11,   5.089775930E+03,   4.097330960E+00</floatArray>
%        </NASA>
%        <NASA Tmax="3500.0" Tmin="1000.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             2.036111160E+00,   1.464541510E-02,  -6.710779150E-06,   1.472229230E-09,
%             -1.257060610E-13,   4.939886140E+03,   1.030536930E+01</floatArray>
%        </NASA>
%      </thermo>
%    </species>
%
%    <species name="N2">
%      <atomArray>N:2 </atomArray>
%      <note>u'121286'</note>
%      <thermo>
%        <NASA Tmax="1000.0" Tmin="300.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             3.298677000E+00,   1.408240400E-03,  -3.963222000E-06,   5.641515000E-09,
%             -2.444854000E-12,  -1.020899900E+03,   3.950372000E+00</floatArray>
%        </NASA>
%        <NASA Tmax="5000.0" Tmin="1000.0" P0="100000.0">
%           <floatArray name="coeffs" size="7">
%             2.926640000E+00,   1.487976800E-03,  -5.684760000E-07,   1.009703800E-10,
%             -6.753351000E-15,  -9.227977000E+02,   5.980528000E+00</floatArray>
%        </NASA>
%      </thermo>
%    </species>
%  </speciesData>
%  <reactionData id="reaction_data">

\end{document}

