\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,enumitem}
\usepackage[pdftex]{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{movie15}
\usepackage{listings}
\usepackage[version=4]{mhchem}
\usepackage[font=footnotesize,format=plain,labelfont=bf]{caption}
\usepackage{tcolorbox}
\tcbuselibrary{theorems}

\newtcbtheorem[number within=section]{mytest}{Test}%
{colback=brown!50!white,colframe=brown!80!black,fonttitle=\bfseries}{th}


\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\input{../xpacc-macros-paper}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\pagestyle{empty}

\begin{center}
\vspace*{1.5in}

\textsc{\color{myBrown} \huge XPACC:  A performance analysis}\\
\bigskip
{\color{myTan} The Center for Exascale Simulation of Plasma-coupled Combustion} \\
{\color{myTan} \textit{University of Illinois at Urbana--Champaign}} \\

\vfil
\begin{minipage}{0.8\textwidth}
This document describes challenges of GPU offloading with external libraries, thread safety, and performance measurements of PC2.

\end{minipage}

\vfil
\end{center}


\newpage
\setcounter{page}{1}
\pagestyle{plain}


\newpage

\section{Performance}
\subsection{vtune for 0.5x}
The top three functions in the 31 species reaction simulation are
\begin{enumerate}\setlength{\itemsep}{0.02in}
\item \texttt{apply\_weno\_js\_dissipation} 
\item \texttt{Cantera::NasaPoly1::updateProperties} (this will be something different for MacArt's)
\item \texttt{exp}
\end{enumerate}
where 1) is 25\% more expense than 2) and 2) 45\% more expensive than 3).  We find that in \texttt{ComputeDVBuffer}, the following call stack is made
\begin{enumerate}\setlength{\itemsep}{0.02in}
\item \texttt{UpdateState}
\item \texttt{Cantera::ThermoPhase::equilibrate}
\item \texttt{Cantera::setState\_HPorUV}
\item \texttt{Canera::ThermoPhase::intEnergy\_mass}
\item \texttt{Canera::ThermoPhase::intEnergy\_mole}
\item \texttt{Cantera::IdealGasPhase::enthalpy\_mole}
\item \texttt{Cantera::IdealGasPhase::\_updateThermo}
\item \texttt{Cantera::GeneralSpeciesThermo::update}
\item \texttt{Cantera::NasaPoly2::updateProperties}
\item \texttt{updateProperties} from within NasaPoly1.h
\end{enumerate}

Thus, the minimal C++ example calling \texttt{intEnergy\_mass()} from the Cantera object shares a similar call stack and can be used to examine the possibility for offloading these calls.  This body of this second-most expensive contributor is given below this.  This function itself is unexceptional however its cost increases due to number of calls
\begin{itemize}
\item in the iterative solver, 
\item scaling with number of species $N_s$, 
\item scaling with grid points, and
\item for WENO and its interface evaluations.
\end{itemize}

\textcolor{red}{Provide more details on the
\begin{itemize}
\item Number of calls being made (point-wise, interface wise)
\item Based on other data, can we estimate how much time is spend in the iterative solver?
\end{itemize}
}

\begin{lstlisting}[language=Python]
    virtual void updateProperties(const doublereal* tt,
                                  doublereal* cp_R, doublereal* h_RT, doublereal* s_R) const {
        doublereal ct0 = m_coeff[0];          // a0
        doublereal ct1 = m_coeff[1]*tt[0];    // a1 * T
        doublereal ct2 = m_coeff[2]*tt[1];    // a2 * T^2
        doublereal ct3 = m_coeff[3]*tt[2];    // a3 * T^3
        doublereal ct4 = m_coeff[4]*tt[3];    // a4 * T^4

        doublereal cp, h, s;
        cp = ct0 + ct1 + ct2 + ct3 + ct4;
        h = ct0 + 0.5*ct1 + 1.0/3.0*ct2 + 0.25*ct3 + 0.2*ct4
            + m_coeff[5]*tt[4];               // last term is a5/T
        s = ct0*tt[5] + ct1 + 0.5*ct2 + 1.0/3.0*ct3
            +0.25*ct4 + m_coeff[6];           // last term is a6

        // return the computed properties in the location in the output
        // arrays for this species
        cp_R[m_index] = cp;
        h_RT[m_index] = h;
        s_R[m_index] = s;
        //writelog("NASA1: for species "+int2str(m_index)+", h_RT = "+
        //    fp2str(h)+"\n");
    }

\end{lstlisting}
Below we give the call stack from when the dependent variables are called before time stepping.  
%\begin{lstlisting}[language=Python]
%I think the species element of \texttt{m\_index} is from a thread-safe build of cantera; but I need to check.  Using gdb and setting a breakpoint at 155 in Nasa1Poly.h we find the call stack
%
%(gdb) bt
%#0  updateProperties (this=0x7fffffff8ad0, tt=0x7fffffff8ad0, cp_R=0x7fffffff8ad0, h_RT=0x7fffffff8ad0, s_R=0x7fffffff8ad0) at include/cantera/thermo/NasaPoly1.h:155
%#1  Cantera::NasaPoly2::updateProperties (this=0x1, tt=0x114cdfe0, cp_R=0x114cdff0, h_RT=0x1151b7b0, s_R=0x114cdfe8) at include/cantera/thermo/NasaPoly2.h:168
%#2  0x0000000010ae4e28 in Cantera::GeneralSpeciesThermo::update (this=0x1, t=1.4340290943268682e-315, cp_R=0x114cdff0, h_RT=0x1151b7b0, s_R=0x114cdfe8)
%    at src/thermo/GeneralSpeciesThermo.cpp:153
%#3  0x00000000108b6e70 in Cantera::IdealGasPhase::_updateThermo (this=0x0) at src/thermo/IdealGasPhase.cpp:380
%#4  0x00000000101f70b8 in Cantera::IdealGasPhase::enthalpy_mole() const ()
%    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/Install/include/cantera/thermo/IdealGasPhase.h:817
%#5  0x0000000010933bb0 in Cantera::ThermoPhase::enthalpy_mass () at include/cantera/thermo/ThermoPhase.h:893
%#6  Cantera::ThermoPhase::report (this=0x0, show_thermo=false, threshold=0)
%    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/basic_string.h:959
%#7  0x0000000010344194 in int euler::util::InitializeRiemannmulti1D<simulation::domain::base<simulation::grid::parallel_blockstructured, simulation::state::base, navierstokes::rhs<simulation::grid::parallel_blockstructured, simulation::state::base, plascom2::operators::stencilset> > >(simulation::domain::base<simulation::grid::parallel_blockstructured, simulation::state::base, navierstokes::rhs<simulation::grid::parallel_blockstructured, simulation::state::base, plascom2::operators::stencilset> >&, int, std::vector<double, std::allocator<double> > const&, std::vector<int, std::allocator<int> > const&, int) ()
%    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/include/EulerUtil.H:5644
%Backtrace stopped: frame did not save the PC
%\end{lstlisting}

\begin{lstlisting}[language=Python]
(gdb) bt
#0  updateProperties (this=0x7fffffff8c10, tt=0x7fffffff8c10, cp_R=0x7fffffff8c10, h_RT=0x7fffffff8c10, s_R=0x7fffffff8c10) at include/cantera/thermo/NasaPoly1.h:155
#1  Cantera::NasaPoly2::updateProperties (this=0x0, tt=0x6600000000, cp_R=0x200000000000, h_RT=0x0, s_R=0x0) at include/cantera/thermo/NasaPoly2.h:168
#2  0x0000000010ae4e28 in Cantera::GeneralSpeciesThermo::update (this=0x0, t=2.1644357067845778e-312, cp_R=0x200000000000, h_RT=0x0, s_R=0x0)
    at src/thermo/GeneralSpeciesThermo.cpp:153
#3  0x00000000108b6e70 in Cantera::IdealGasPhase::_updateThermo (this=0x0) at src/thermo/IdealGasPhase.cpp:380
#4  0x00000000101f70b8 in Cantera::IdealGasPhase::enthalpy_mole() const ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/Install/include/cantera/thermo/IdealGasPhase.h:817
#5  0x00000000101f7138 in Cantera::ThermoPhase::intEnergy_mole() const ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/Install/include/cantera/thermo/ThermoPhase.h:233
#6  0x000000001092bd7c in Cantera::ThermoPhase::intEnergy_mass () at include/cantera/thermo/ThermoPhase.h:900
#7  Cantera::ThermoPhase::setState_HPorUV (this=0x20, Htarget=6.9533558063899611e-310, p=1.4344713028425219e-315, dTtol=6.9533558063915421e-310, doUV=16)
    at src/thermo/ThermoPhase.cpp:251
#8  0x000000001092e09c in ~allocator (this=0x7fffffff91b0) at src/thermo/ThermoPhase.cpp:203
#9  ~_Vector_base (this=0x7fffffff91b0) at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_vector.h:79
#10 ~vector (this=0x7fffffff91b0) at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_vector.h:424
#11 Cantera::ThermoPhase::equilibrate (this=0x7fffffff9368, XY=..., solver=..., rtol=0, max_steps=0, max_iter=0, estimate_equil=0, log_level=0)
    at include/cantera/equil/MultiPhase.h:636
#12 0x00000000103a4afc in eos::multi_species_cantera::UpdateState(unsigned long) ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/src/EOS.C:949
#13 0x00000000103a1380 in eos::eos::ComputeDVBuffer(ix::util::sizeextent const&, std::vector<unsigned long, std::allocator<unsigned long> > const&) ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/src/EOS.C:328
#14 0x000000001036b25c in navierstokes::rhs<simulation::grid::parallel_blockstructured, simulation::state::base, plascom2::operators::stencilset>::ComputeDV(int) ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/include/NavierStokesRHS.H:2090
#15 0x0000000010308d84 in simulation::domain::base<simulation::grid::parallel_blockstructured, simulation::state::base, navierstokes::rhs<simulation::grid::parallel_blockstructured, simulation::state::base, plascom2::operators::stencilset> >::ComputeDV(int) ()
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_vector.h:780
#16 0x00000000102f1c00 in plascom2::application::RunPlasCom2() () at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/src/RunPlasCom2.C:609
#17 0x0000000010279aa4 in plascom2::application::RunApplication() () at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/src/PlasCom2.C:30
#18 0x00000000101482c4 in main () at /usr/workspace/wsa/xpacc/Users/buchta2/lassen-Support/Build-2019-03-07.00/PlasCom2/include/Application.H:31
\end{lstlisting}


In my c++ example the call stack is given

\begin{lstlisting}[language=Python]
#0  updateProperties (this=0x3fffffffb430, tt=0x3fffffffb430, cp_R=0x3fffffffb430, h_RT=0x3fffffffb430, s_R=0x3fffffffb430) at include/cantera/thermo/NasaPoly1.h:155
#1  Cantera::NasaPoly2::updateProperties (this=0x10c21c00, tt=0x10c21c08, cp_R=0x26388280, h_RT=0x0, s_R=0x3fffffffb430) at include/cantera/thermo/NasaPoly2.h:168
#2  0x0000000010337f48 in Cantera::GeneralSpeciesThermo::update (this=0x10c21c00, t=1.3890982506657935e-315, cp_R=0x26388280, h_RT=0x0, s_R=0x3fffffffb430)
    at src/thermo/GeneralSpeciesThermo.cpp:153
#3  0x000000001008f4d8 in Cantera::IdealGasPhase::_updateThermo (this=0x35) at src/thermo/IdealGasPhase.cpp:380
#4  0x000000001005865c in Cantera::IdealGasPhase::enthalpy_RT_ref (this=0x10c21c00)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/IdealGasPhase.h:817
#5  0x000000001005b2d4 in Cantera::IdealGasPhase::enthalpy_mole (this=0x10c21c00)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/IdealGasPhase.h:385
#6  0x000000001005b1c8 in Cantera::ThermoPhase::intEnergy_mole (this=0x10c21c00)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/ThermoPhase.h:233
#7  0x000000001004524c in Cantera::ThermoPhase::intEnergy_mass (this=0x10c21c00)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/ThermoPhase.h:900
#8  0x00000000100513c0 in main () at main.cpp:36
(gdb) 
\end{lstlisting}


\section{Multi-threaded tests}
Below I detail two observations related to failurs of running multi-threaded Cantera cases in PC2 and a standalone Cantera code.
\begin{enumerate}
\item Minimum working example (MWE) Cantera will show that multiple calls to setState\_TPY and computing the density and internal energy given mass fraction, pressure, and temperature will fail intermittantly on program execution.
\item In PC2 (enable OMP or hydra), multi-component configurations fails due to Cantera throwing an error.    Backtracing the error in gdb shows that its making calls to Cantera with what appears to be unitilized data. Branches that exhibit this behavior are
\begin{itemize}
\item protoy5 (-DENABLE\_OPENMP=1)
\item hyb5 (and hyb5\_unimem) with -DUSE\_HYDRA=1
\end{itemize}
\end{enumerate}

\subsection{Basic \textit{Cantera} example}
Despite compiling \textit{Cantera} \textit{build\_thread\_safe}, which requires boost libraries,
\begin{lstlisting}[language=Python]
CXX = 'xlC_r'
CC = 'xlc_r'
prefix = '/usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install'
python_package = 'none'
python3_package = 'n'
f90_interface = 'n'
FORTRAN = 'xlf_r'
use_sundials = 'n'
lapack_ftn_trailing_underscore = False
cxx_flags = '-std=c++11'
build_thread_safe = True
boost_inc_dir = '/p/gscratchr/buchta2/spack/opt/spack/linux-rhel7-ppc64le/gcc-4.9.3/boost-1.69.0-htene6dwkpr7zb3fi6x63gvopqlrg4yo/include/'
boost_lib_dir = '/p/gscratchr/buchta2/spack/opt/spack/linux-rhel7-ppc64le/gcc-4.9.3/boost-1.69.0-htene6dwkpr7zb3fi6x63gvopqlrg4yo/lib/'
build_with_f2c = False
\end{lstlisting}

a minimum working example of \textit{Cantera} is provided.  Depending on the number of threads set by environment variable, the MWE will segmentation fault.  

\begin{lstlisting}[language=C++]
#include <iostream>
#include <string>
#include "cantera/IdealGasMix.h"

int main() {

 std::cout.precision( 4 );
 std::cout.setf( std::ios::scientific );

 Cantera::IdealGasMix gas( "gri30.xml", "gri30" );
 int kk = gas.nSpecies();
 std::vector<double> wts( kk, 0.0 );
 double totalMass = 1.0;
 std::vector<double> mf( kk, 1./double(kk) );
 double temperature=298;
 double pressure=101325.;
 double rho=0.;
 double internalEnergy=0.;

 gas.getMolecularWeights( &wts[0] );
 gas.setState_TPY(temperature,pressure,&mf[0]);
#pragma omp parallel for
 for(int i=0; i < 100; i++){
    temperature=298.;
    gas.setState_TPY(temperature,pressure,&mf[0]);
    rho = gas.density();
    internalEnergy = rho*gas.intEnergy_mass();
}


}
\end{lstlisting}

The following script executes execution one-hundred times, the code will segmentation approximately five times out of one hundred.  

\begin{lstlisting}[language=bash]
#!/bin/bash
threads=20
echo "test will print if any seg faults occur in MWE cantera example"
echo "number of threads set: $threads"
export OMP_NUM_THREADS=$threads
file=output
if [ -f $file ] ; then
    rm $file
fi
for i in {1..100}
do
./testCantera.exe
echo $i
done
\end{lstlisting}

Example output
\begin{lstlisting}[language=bash]
bash-4.2$ sh runTest 
test will print if any seg faults occur in MWE cantera example
number of threads set: 20
1
2
3
4
5
6
7
8
9
10
11
12
13
14
runTest: line 11:  6093 Segmentation fault      (core dumped) ./testCantera.exe > output
15
16
17
18
\end{lstlisting}

Backtrace of one of these segmentation faults gives

\begin{lstlisting}[language=bash]
(gdb) bt
#0  std::local_Rb_tree_rotate_left (__x=__x@entry=0x100164004a00, __root=@0x3fffffffafd8: 0x100040004a00)
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/src/c++98/tree.cc:138
#1  0x0000100001188474 in std::_Rb_tree_insert_and_rebalance (__insert_left=<optimized out>, __x=0x3fffffffafd0, __p=<optimized out>, __header=...)
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/src/c++98/tree.cc:278
#2  0x000000001008f174 in std::_Rb_tree<int, std::pair<int const, Cantera::CachedValue<double> >, std::_Select1st<std::pair<int const, Cantera::CachedValue<double> > >, std::less<int>, std::allocator<std::pair<int const, Cantera::CachedValue<double> > > >::_M_insert_node (this=0x48, __x=0x1000257fe1a0, __p=0x0, __z=0x3fffffffafc0)
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_tree.h:1696
#3  0x000000001008e124 in std::_Rb_tree<int, std::pair<int const, Cantera::CachedValue<double> >, std::_Select1st<std::pair<int const, Cantera::CachedValue<double> > >, std::less<int>, std::allocator<std::pair<int const, Cantera::CachedValue<double> > > >::_M_emplace_hint_unique<std::piecewise_construct_t const&, std::tuple<int const&>, std::tuple<> >(std::_Rb_tree_const_iterator<std::pair<int const, Cantera::CachedValue<double> > >, std::piecewise_construct_t const&, std::tuple<int const&>&&, std::tuple<>&&) (this=0x0, 
    __pos=..., __args=..., __args=..., __args=...)
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_tree.h:1797
#4  0x000000001008cfe4 in Cantera::ValueCache::getScalar (id=1)
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_map.h:501
#5  Cantera::IdealGasPhase::_updateThermo (this=0x0) at src/thermo/IdealGasPhase.cpp:374
#6  0x000000001004ce94 in Cantera::IdealGasPhase::enthalpy_RT_ref (this=0x3fffffffafc0)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/IdealGasPhase.h:817
#7  0x000000001004fc14 in Cantera::IdealGasPhase::enthalpy_mole (this=0x3fffffffafc0)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/IdealGasPhase.h:385
#8  0x000000001004fb08 in Cantera::ThermoPhase::intEnergy_mole (this=0x3fffffffafc0)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/ThermoPhase.h:233
#9  0x000000001005a54c in Cantera::ThermoPhase::intEnergy_mass (this=0x3fffffffafc0)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/Install/include/cantera/thermo/ThermoPhase.h:900
#10 0x000000001005af24 in __xl_main_l21_OL_1_OL_2 () at main.cpp:27
#11 0x000000001005b204 in __xl_main_l21_OL_1 () at main.cpp:23
#12 0x0000100000e6c124 in ?? () from /usr/tce/packages/xl/xl-2019.02.07/lib/libxlsmp.so.1
#13 0x0000100001388728 in start_thread () from /lib64/libpthread.so.0
#14 0x00001000014dd250 in clone () from /lib64/libc.so.6
(gdb) 
\end{lstlisting}

An alternative minimum example, which successfully runs, without segmentation fault, but with low performance, creates numThread Cantera objects before the parallel region begins then access a thread specific object in the parallel for loop.
\begin{lstlisting}[language=C++]
#include <iostream>
#include <string>
#include "cantera/IdealGasMix.h"
#include <omp.h>


int main() {

 std::cout.precision( 4 );
 std::cout.setf( std::ios::scientific );

 int numThreads=10;
 std::vector<Cantera::IdealGasMix> gasThdPvt;
 Cantera::IdealGasMix gas( "gri30.xml", "gri30" );
 for (int i=0; i < numThreads ; i++){
  gasThdPvt.push_back(gas);
 }
 std::cout << "Instantiated " << numThreads << "cantera objects" << std::endl;

 int kk = gasThdPvt[0].nSpecies();
 std::vector<double> mf( kk, 1./double(kk) );
 double temperature=298;
 double pressure=101325.;
 double rho=0.;
 double internalEnergy=0.;
 for (int i=0; i < numThreads ; i++){
  gasThdPvt[i].setState_TPY(temperature,pressure,&mf[0]);
 }

#pragma omp parallel for
 for(int i=0; i < 100; i++){
  int t=omp_get_thread_num();
  std::cout << i << "," << t << std::endl;
  rho = gasThdPvt[t].density();
  internalEnergy = rho*gasThdPvt[t].intEnergy_mass();
}


}

\pagebreak
\subsection{Multicomponent WENO shock tube: }
An two-dimensional, nominally 1D shock tube $(N_x,N_y)=(100,10)$, WENO test case Testing/Data/WENOMulti/C2H4O2 is used to facilitate debugging a multi-thread, multi-component WENO test case so that we can run the overset mesh Y5 prediction runs with plasma-coupled combustion.  This actually uncovered a 2D-specific issue with hydra which Matthias has fixed.  

However, the case will fails to run if $\mathrm{OMP\_NUM\_THREADS} > 1$ (and HYB\_CPU $> 1$ and HYB\_GPUS $> 1$).  An initial gdb run shows the following

\begin{lstlisting}[language=bash]
gdb ./plascom2x-omp 
(gdb) r -c riemann.test.config 
\end{lstlisting}

which fails with

\begin{lstlisting}[]
plascom2x-omp: Speed of sound: 	1.18322
plascom2x-omp: Cp: 	4.61965e+281
plascom2x-omp: Cv: 	6.77196e+256
plascom2x-omp: NavierStokesRHS: Viscous fluxes are OFF.
plascom2x-omp: NavierStokesRHS: DV exchange is ON.
plascom2x-omp: NavierStokesRHS: Flux exchange is ON.
plascom2x-omp: NavierStokesRHS: WENO is ON.
plascom2x-omp: NavierStokesRHS: WENO is set to Pooya.
[New Thread 0x10000d77f0d0 (LWP 71127)]
plascom2x-omp: Computing DV before stepping.
terminate called recursively
terminate called after throwing an instance of 'Cantera::CanteraError'

Program received signal SIGABRT, Aborted.
0x000010000145eb18 in raise () from /lib64/libc.so.6
Missing separate debuginfos, use: debuginfo-install glibc-2.17-157.el7_3.5.ppc64le libibumad-1.3.10.2.MLNX20150406.966500d-0.1.34100.ppc64le libibverbs-1.2.1mlnx1-OFED.3.4.0.1.4.34204.ppc64le libmlx4-1.2.1mlnx1-OFED.3.4.0.0.4.34204.ppc64le libmlx5-1.2.1mlnx1-OFED.3.4.1.0.0.34204.ppc64le libnl-1.1.4-3.el7.ppc64le librdmacm-1.1.0mlnx-OFED.3.4.0.0.4.34204.ppc64le numactl-libs-2.0.9-6.el7_2.ppc64le opensm-libs-4.8.0.MLNX20161013.9b1a49b-0.1.34204.ppc64le zlib-1.2.7-17.el7.ppc64le
\end{lstlisting}

executing backtrace in gbd gives which indicates that unitilized variables are in UpdateState and setState\_UV given by \#8 and \#9.
\begin{lstlisting}[language=bash]
(gdb) bt
#0  0x000010000145eb18 in raise () from /lib64/libc.so.6
#1  0x0000100001460c9c in abort () from /lib64/libc.so.6
#2  0x00001000012ac3f8 in __gnu_cxx::__verbose_terminate_handler ()
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/libsupc++/vterminate.cc:50
#3  0x00001000012a8ea4 in __cxxabiv1::__terminate (handler=<optimized out>)
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/libsupc++/eh_terminate.cc:47
#4  0x00001000012a8f60 in std::terminate ()
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/libsupc++/eh_terminate.cc:57
#5  0x00001000012a93a0 in __cxxabiv1::__cxa_throw (obj=0x121242e0, tinfo=0x112aa650 <typeinfo for Cantera::CanteraError>, 
    dest=0x1025f120 <Cantera::CanteraError::~CanteraError()>)
    at /builddir/build/BUILD/gccspack/spack/var/spack/stage/gcc-4.9.3-wrtcv2vmjcdbqawlku4lfzmeploo5uuw/gcc-4.9.3/libstdc++-v3/libsupc++/eh_throw.cc:87
#6  0x0000000010a9e558 in ~_Vector_base (this=0x3fffffff5d20) at src/thermo/ThermoPhase.cpp:397
#7  std::vector<Cantera::XML_Node const*, std::allocator<Cantera::XML_Node const*> >::~vector (this=0x11edf8d0)
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_vector.h:424
#8  0x0000000010aa0acc in Cantera::ThermoPhase::setState_UV (this=0x3fffffff5e70, u=297.99996154475684, v=1.3503724960265941e-315, dTtol=1.4267464876566648e-315)
    at src/thermo/ThermoPhase.cpp:203
#9  0x00000000104a8224 in eos::multi_species_cantera::UpdateState (this=0x3fffffff5e70, index=293372688)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/src/EOS.C:949
#10 0x00000000104a3180 in eos::eos::ComputeDVBuffer (this=0x11696350, regionInterval=..., bufferSizes=...)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/src/EOS.C:328
#11 0x00000000104452a8 in navierstokes::rhs<simulation::grid::parallel_blockstructured, simulation::state::base, plascom2::operators::stencilset>::ComputeDV (this=0x117b58b0, 
    threadId=0) at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/include/NavierStokesRHS.H:2002
#12 0x000000001045b9dc in __gnu_cxx::__normal_iterator<int*, std::vector<int, std::allocator<int> > >::__normal_iterator ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/include/Domain.H:2743
#13 __gnu_cxx::__normal_iterator<int*, std::vector<int, std::allocator<int> > >::operator++ ()
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_iterator.h:757
#14 ComputeDV (this=0x0, threadId=292099072) at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/include/Domain.H:2741
#15 __xl__ZN8plascom211application11RunPlasCom2Ev_l784_OL_1 (this=0x1, this=0x1, this=0x1, this=0x1, this=0x1, this=0x1, this=0x1, this=0x1, this=0x1, 
    send=@0x419: <error reading variable>, recv=@0x10f58d70: -0, dt=@0x61: <error reading variable>, op=@0x3fffffff9380: 4294940464, this=0x1, 
    send=@0x419: <error reading variable>, recv=@0x10f58d70: -0, dt=@0x61: <error reading variable>, op=@0x3fffffff9380: 4294940464, this=0x1, __pf=0x1000015f0750 <main_arena>, 
    __os=..., __str=..., this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __f=9.9307194814090555e-322, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, localGridIndex=22721872, this=0x1, __n=70368744150896, __n=70368744150896, __os=..., 
    __str=..., this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, __n=70368744150896, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, inStep=16383, this=0x1, __f=9.9307194814090555e-322, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __f=9.9307194814090555e-322, 
    this=0x1, __pf=0x1000015f0750 <main_arena>, __os=..., __str=..., this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, inTime=0)
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/src/RunPlasCom2.C:606
#16 0x0000100000fcb77c in _lomp_Parallel_StartDefault_Fast () from /usr/tce/packages/xl/xl-2019.02.07/lib/libxlsmp.so.1
#17 0x00000000103b1d04 in __gnu_cxx::__normal_iterator<std::string*, std::vector<std::string, std::allocator<std::string> > >::base ()
    at /usr/workspace/wsa/xpacc/Users/buchta2/ray-Support/Build-2019-03-22.01/PlasCom2/src/RunPlasCom2.C:465
#18 operator!=<const std::basic_string<char> *, std::basic_string<char> *, std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > > ()
    at /usr/tce/packages/gcc/gcc-4.9.3/gnu/lib64/gcc/powerpc64le-unknown-linux-gnu/4.9.3/../../../../include/c++/4.9.3/bits/stl_iterator.h:794
#19 plascom2::application::RunPlasCom2 (this=0x1, this=0x1, send=@0x419: <error reading variable>, recv=@0x10f58d70: -0, dt=@0x61: <error reading variable>, 
    op=@0x3fffffff9380: 4294940464, this=0x1, send=@0x419: <error reading variable>, recv=@0x10f58d70: -0, dt=@0x61: <error reading variable>, op=@0x3fffffff9380: 4294940464, 
    this=0x1, __pf=0x1000015f0750 <main_arena>, __os=..., __str=..., this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __f=9.9307194814090555e-322, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, localGridIndex=22721872, this=0x1, 
    __n=70368744150896, __n=70368744150896, __os=..., __str=..., this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, 
    __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, this=0x1, __pf=0x1000015f0750 <main_arena>, __n=70368744150896, this=0x1, 
---Type <return> to continue, or q <return> to quit---
\end{lstlisting}

Using the following command to set a breakpoint before the UpdateState failure.  Subsequent command show that values are untiliziaed.  
\begin{lstlisting}[language=bash]
gdb ./plascom2x-omp
(gdb) break EOS.C:328
(gdb) r -c riemann.test.config
(gdb) print xyzIndex
No symbol "xyzIndex" in current context.
(gdb) list
323	      for(size_t iY = yStart;iY <= yEnd;iY++){
324	        size_t yzIndex = iY*xSize+zIndex;
325	        for(size_t iX = xStart;iX <= xEnd;iX++){
326	          size_t xyzIndex = yzIndex + iX;
327	          
328	          UpdateState(xyzIndex);
329	          ComputeTemperature(xyzIndex);
330	          ComputePressure(xyzIndex);
331	          ComputeGamma(xyzIndex);
332	        }
(gdb) print xStart
$1 = 292159520
(gdb) step
289	    numDim = bufferSizes.size();
(gdb) 
No symbol "xyzIndex" in current context.
(gdb) step
324	        size_t yzIndex = iY*xSize+zIndex;
(gdb) print xSize
$4 = 2
(gdb) print zIndex
No symbol "zIndex" in current context.
(gdb) step
328	          UpdateState(xyzIndex);
(gdb) print yzIndex
$5 = 17592412003424
(gdb) 
\end{lstlisting}



\subsection{Performance: OpenMP and \textit{Hydra}}
\begin{figure}
\centering\includegraphics[page=1]{/Users/davidbuchta/Downloads/weak-scaling-data.pdf}
\caption{Single node performance of right-hand side time.  At $\approx64^3$ points, the performance between GPU and 20 CPU becomes comparable.}
\label{fig:sbp:ray}
\end{figure}

\begin{figure}
\centering\includegraphics[page=2]{/Users/davidbuchta/Downloads/weak-scaling-data.pdf}
\caption{Single node performance of right-hand side time}
\label{fig:weno:ray}
\end{figure}


\end{document}

