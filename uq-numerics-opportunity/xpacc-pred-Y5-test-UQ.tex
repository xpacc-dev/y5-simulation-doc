\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,enumitem}
\usepackage[pdftex]{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{movie15}
\usepackage[version=4]{mhchem}
\usepackage[font=footnotesize,format=plain,labelfont=bf]{caption}


\input{../xpacc-macros}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\pagestyle{empty}

\begin{center}
\vspace*{1.5in}

\textsc{\color{myBrown} \huge XPACC:  Example propagation of discretization and parametric uncertainty}\\
\bigskip
{\color{myTan} The Center for Exascale Simulation of Plasma-coupled Combustion} \\
{\color{myTan} \textit{University of Illinois at Urbana--Champaign}} \\

\vfil
\begin{minipage}{0.8\textwidth}
This document illustrates propagating discretization effects with parametric uncertainty using first-order ANOVA expansion.

\end{minipage}

\vfil
\end{center}


\newpage
\setcounter{page}{1}
\pagestyle{plain}


\newpage

Critiques from JBF
\begin{itemize}
\item We do not leverage theoretical error estimation (of the numerics) for its output variance.  Or rather can we leverage this?  It depends if your nominal mesh is in the asymptotic convergence limit for your QoI.  You would not need 3 points if your in the asymptotic limit to define, compute or estimate a $\sigma_\Delta$.  Think Richardson extrapolation as a way to leverage numerics.  
\item Numerics should not be treated like a random variable.  How can you treat it like a PDF?  Might your PDF be a $\delta(\Delta t)$?  Or shouldn't it include cost?  
\item This approach might be useful if you consider higher-order interactions in ANOVA expansion.  This would provide a better estimate of output variance than convergence study on nominal input parameters.  
\item This approach might be useful if you can't leverage theoretical error estimation for general curvilinear, overset, multi-physics simulations.
\end{itemize}



\section{Propagating effects of numerics to QoI}
Predictions involves simulations at nominal input values, including numerical resolution, where the resolution is usually guided by \textit{anticipated} important physical scales and computational tractability. However, coupled multi-physics, multi-scale simulations and limited computer resources can prohibit sufficient resolution of all possible interactions. These interactions are also not known \textit{a priori}; hence the simulation.  One might accomodate this resolution-uncertainty need by embedding a factor $F$ into space discretization, yet this incurs a cost of $\approx (1+F)^3$ and having to take $\approx (1+F)$ times more time steps to attain the solution at the same time $T$ (for numerical stabillity based on convection for example). Furthermore, propagating the effects of numerics with uncertain input parameters is an additional challenge. 
%\textcolor{red}{What about automatic numerics refinement and error control in time integrators? What about \textit{a posterior} error estimation?  Don Estep (and probably many others) do this type of work. It may be intrusive or not possible for the numerics and complicated overset meshes we use.}
%For example, rare and intense turbulence fluctuations can couple to strong species gradients to extinguish or promote ignition, possibly in error, despite accomodating `anticipated' resolution needs.  

Predictions with quantified uncertainty involve perturbations to inputs for quantifying output variance.  Thus, the space of possible interactions and states grows with number of uncertain parameters and the perturbations to them.  Likewise, simulation convergence is input dependent, so propagation of numerics errors with parametric uncertainty is challenging and expensive. As mentioned numerics are another input parameter which can interact with other input parameters. It is possible to probe local sensitivity to support relative insensitivity to numerics, but this approach would fail to provide a picture of the family of error curves over the range of parameters (e.g. a global effect). 

Another challenge is error estimation for complicated physics and discretizations (e.g. overset, general curvilinear meshes with ranges of resolutions as well as mesh interpolation between them). Such situations can preclude output useful variance estimates by applying theoretical error estimates. 

Thus we use a model problem where numerics parameters are considered within the UQ framework just like other uncertain parameters. They are perturbations to a nominal resolution, which is computationally tractable for i) a forward solution, ii) perturbations to the mesh, and iii) perturbations to the parameters (with mesh perturbations). These three features make it possible to quantify and propagate parametric uncertainty as well as quantify mesh effects on quantities of interest. 

%Propagation of numerics errors are typically not included in simulation prediction.  Instead, local sensitivity is evaluated to support relative insensitivity to changes in the numerics, compared for example to other parameteric or model-form uncertainty.  Thus, numerics effects are not directly accounted in the final reported output variance.  In the current model problem, numerics parameters are considered within the UQ framework similar to uncertain 

We begin with an ordinary differential equation and initial condition
\begin{align}
\frac{d q}{dt} &= \lambda q\\ 
q(t=0)&=q_o,
\end{align}
\noindent where $(\lambda,q_o)$ are uncertain parameters which vary by $\lambda=[-1,0.1]$ and $q_o=[0,1]$.  Although the exact solution to (1) with initial condition (2) is known, $q=q_o \exp(\lambda t)$, we consider the mixture of two effects on the solution $q_N=q(t=T;\lambda,q_o)$: i) time-discretization 
\begin{equation}\label{eq:rk4}
q_{n+1} = q_n\left(1 + \Delta t \lambda + \frac{1}{2} (\Delta t \lambda)^2 + \frac{1}{6} (\Delta t \lambda)^3 + \frac{1}{24} (\Delta t \lambda)^4\right),
\end{equation}
which is given as a standard fourth-order Runge--Kutta method with step size $\Delta t$, and ii) the uncertain parameters $\lambda$ and $q_o$.  The time-step range is chosen to be large enough for stability of the numerical solution ($\Delta t \lambda=2.78$ for \ref{eq:rk4}) and small enough based on computational expense.  Given nominal values of uncertain parameters, the effect of time resolution will be incorporated into the prediction of our solution $q_N\pm\sigma$ where $\sigma^2 = \sigma^2_\lambda + \sigma^2_{q_o}+ \sigma^2_{\Delta t}$.

The probability distributions for uncertain inputs  are chosen as follows:
\begin{align}
p(\lambda) &= \frac{11}{10}\left[H(\lambda+1)-H(\lambda-0.1) \right]\\
p(q_o) &= H(q_o)-H(q_o-1)\\ \label{eq:refexpdist}
p(\Delta t) &= A\exp(-\Delta t)+B,
\end{align}
\noindent where $H(\cdot)$ is the Heaviside function, $A=3.63$, and $B=-0.225$ which yields $p(\Delta t=2.78)=0$ and
\begin{equation}
\frac{1}{2.78}\int_0^{2.78} p(\Delta t) \Delta t=1.
\end{equation}
\noindent The exponential distribution is chosen for $\Delta t$ for weighting more accuracy favorable.  Although, it can be argued that this would incur significant computational cost. Ultimately, the choice for this demo is immaterial. For purposes of integer values of $N$ steps, which evenly divide the time domain $t=[0,10]$ we consider a discrete normalized probability distribution, modeled after the exponential in (\ref{eq:refexpdist})
\begin{align}\nonumber 
p(\Delta t=1) &= 0.147 \\ \label{eq:discretePDF} 
p(\Delta t=0.1) &= 0.406 \\ 
p(\Delta t=0.01) &= 0.447. \nonumber
\end{align}
\noindent Sensitivity indices for $\Delta t, \lambda, q_o$ are computed by the following first-order ANOVA expansion
\begin{equation}
q_N(\mathbf{x}) = q_N(\mathbf{x}_o) + \sum_{i=1}^{N_p} q_{N}(x_i)
\end{equation}
with variance
\begin{align}
%\overline{q_N} &= \int_{-1}^1 q^i_{N}(x_i) p(x_i) d x_i \approx \sum_{k=1}^3 w_k q^i_{N}(x_i^k) p(x_i^k)\\
\sigma^2_{x_i} &= \int_{-1}^1 \left[ q_{N}(x_i)-q_N(\mathbf{x}_o)\right]^2 p(x_i)\,d x_i \approx \sum_{k=1}^3 w_k \left[ q_{N}(x_i^k)-q_N(\mathbf{x}_o)\right]^2 p(x_i^k), \label{eq:variance}
\end{align}
\noindent approximated from the three-point Gauss--Lobatto quadrature rule with $\mathbf{w}=[\frac{1}{3},\frac{4}{3},\frac{1}{3}]$ for quadrature points $\mathbf{x}=[-1,0,1]$. Using change of variable, $x_i= \frac{a-b}{2}\tilde{x_i} + \frac{a+b}{2}$, the bounds of integration can be modified from [a,b] to [-1,1] for \ref{eq:variance}.  Details of each variance calculation are shown
\begin{align}
\sigma^2_{\Delta t} &\approx \ln(10) \exp[\ln(10)]\times \sum_{k=1}^3 w_k \exp(\tilde{x}_k-1) \left[q_N(\tilde{x}_k)-q_N(\mathbf{x}_o) \right]^2 p(\tilde{x}_k)\\
\sigma^2_{q_o} &\approx \frac{1}{2}\sum_{k=1}^3 w_k \left[q_N(q_{o,k})-q_N(\mathbf{x}_o) \right]^2 p(q_{o,k})\\
\sigma^2_{\lambda} &\approx  \frac{11}{20}\sum_{k=1}^3 w_k \left[q_N(\lambda_k)-q_N(\mathbf{x}_o) \right]^2 p(\lambda_k),
\end{align}
\noindent where $\tilde{x}=\log_{10}(\Delta t)+1$.  For the nominal values of $(q_o,\lambda,dt)=(0.5,-0.45,0.1)$, the variances are listed in table \ref{tab:var}.  Based on the ranges of parameters considered, it might be anticipated that $\lambda$ would have a large effect on the final solution value over the period.  Likewise, the accuracy of fourth-order Runge--Kutta at nominal values of $\Delta t=0.1$ have a small effect on the solution.  However, repeating this analysis with second-order Runge--Kutta, $q_{n+1} = q_n\left(1 + \Delta t \lambda + \frac{1}{2} (\Delta t \lambda)^2 \right)$, shows increase in sensitivity on the order of the initial condition uncertain parameter (e.g $10^{-5}$).

\begin{table}[h!]
\begin{center}
\begin{tabular}{c c c}
\hline 
Variable & Variance (RK4) & Variance (RK2)  \\\hline
$\sigma^2_{q_o}$ & $2.1\times10^{-5}$ & $3.1\times10^{-5}$\\
$\sigma^2_{\lambda}$ & $0.31$ & 0.30  \\
$\sigma^2_{\Delta t}$ & $2.5\times10^{-9} $ & $2.6\times10^{-5} $\\\hline
%$\sigma^2_{\Delta t}$ & $2.6\times10^{-5} $
\end{tabular} 
\caption{Effects of time-discretization and parametric uncertainty on the first-order ANOVA variance components for output $q_N=q_N(t=T)$.}
\label{tab:var}
\end{center}
\end{table}  


To-do items for xpacc
\begin{itemize} 
\item Next: Incorporate numerics effects in 1D plasma-coupled combustion ethylene flame simulations UQ as examining it for our full Y5. I suspect for certain quantities of interest (flame thickness or propagation speed) that the discretization will have a comparable effect to other uncertain quantities (e.g. reaction rates or transport) and will in a sense guide our meshes.  I hope this is moving toward coding it near PC2.  I would love to have sensitivty analysis and propagation near PC2 with associated tests!!
\end{itemize}
To-do items for research on this toy problem
\begin{itemize}
\item Assess higher-order interactions.  Equation \ref{eq:rk4} clearly has them.
\item Confirm (or refute) relative ordering with local sensitivity (e.g. $\sigma_i \partial q_N/\partial \Delta t$) where $\sigma_i$ is the variance of the parameter from their probability distribution.
\item Confirm results as your shrink parameteric uncertainty ($q_o, \lambda$), they will become order of variance due to $\Delta t$; I did this to a degree by considering $RK2$ method and it works as one might expect. 
\end{itemize}

%Furthermore, without loss of generality, the $\Delta t$ sequence could be $\Delta t= [1,0.5,0.25]$ (which might be useful for grid-refinemnt purposes) , in which case $\log_2(\Delta t)=[0,-1,-2]$ would be used for the uncertain variable.

%Likewise we can consider the following,
%
%\begin{align}\nonumber 
%p(\Delta t=2) &= 0.022 \\ \nonumber 
%p(\Delta t=1) &= 0.092 \\ \label{eq:discretePDF}
%p(\Delta t=0.5) &= 0.16 \\ \nonumber 
%p(\Delta t=0.25) &= 0.21\\ \nonumber 
%p(\Delta t=0.125) &= 0.25\\ \nonumber 
%p(\Delta t=0.0625) &= 0.26. 
%\end{align}

%\section{Local sensitivity}
%Local sensitivity of the solution is considered for nominal values of the uncertain parameters $\Delta t=0.1$, $\lambda=-0.45$, and $q_o=0.5$.

%\section{More certain paramters and numerical discretization}
%As shown previously, for an uncertain parameter, or more specifically large values of output sensitivity times the %arameter uncertainty, numerical effects are swamped by parametric effects.  Refining parameter unceratinty further, we can find that 

%\section{Application to multi-physics flow problem}
%The following illustrates effects of parameteric uncertainty and mesh resolution on a complex compressible, reacting flow problem.  The cross-stream velocity above the flame-holder cavity is $M=3.1$.  Two configurations of sonic, ethylene fuel injection is considered:  vertical injection, $5$ mm from the front cavity wall, and horizontal injection $4$ mm above the bottom cavity wall.  To assess mesh resolution effects, in conjunction with parametric uncertainty with regard to most-sensitive chemical kinetics and laser-induced breakdown parameters, three meshes are considered with increasing mesh resolution which necessarily reduces the time step for stability.  The choice of grid resolution is chosen based on boundary-layer resolution needs as well as balancing computational cost both in time-per-time step and time-to-solution.   Application to three-dimensional flow is the same although more computational expensive.


%Sensitivity to this choice of minimum value will be shown; however, for small $\Delta t$, such that $q_N$ is insensitive to refinement, the sensitivity is dominated by parametric uncertainty alone. Finally






\end{document}

