\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,enumitem}
\usepackage[pdftex]{graphicx}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{movie15}
\usepackage[version=4]{mhchem}
\usepackage[font=footnotesize,format=plain,labelfont=bf]{caption}


\input{../xpacc-macros}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\pagestyle{empty}

\begin{center}
\vspace*{1.5in}

\textsc{\color{myBrown} \huge XPACC:  Y4-5 Simulation Prediction}\\
\bigskip
{\color{myTan} The Center for Exascale Simulation of Plasma-coupled Combustion} \\
{\color{myTan} \textit{University of Illinois at Urbana--Champaign}} \\

\vfil
\begin{minipage}{0.8\textwidth}
This document describes the numerical simulations of the prediction target and those supporting simulations used, in part, for the uncertainty quantification analysis. The final prediction results for the ACT-II target are also discussed. 

\end{minipage}

\vfil
\end{center}


\newpage
\setcounter{page}{1}
\pagestyle{plain}


\newpage



\section{Appendix: Governing equations and solution methods}
The conservation equations for $\vec{Q}=[\rho, \rho u, \rho v, \rho w, E, \rho Y_k ]^T$ in transformed coordinates are
\begin{equation}
\frac{\partial \vec{Q}}{\partial t} + J \frac{\partial}{\partial \xi_i} \left [M_{ij} \left(\vec{F}_j^I - \vec{F}_j^V \right) \right] = \vec{S}
\end{equation}
\noindent where $J$ is the Jacobian of the mesh transformation $\xi_i=\Xi_i(\vec{x})$ and $x_i=X_i(\vec{\xi})$ with metrics
\begin{equation}
M_{ij} = J^{-1} \frac{\partial \Xi_i}{\partial x_j} = \underbrace{\frac{1}{2}\epsilon_{jqr}\epsilon_{ist} \frac{\partial X_q}{\partial \xi_s} \frac{\partial X_r}{\partial \xi_t}}_{\textrm{check this; re-expr. from Ram}}.
\end{equation}
\noindent The inviscid and viscid fluxes are
\begin{equation}
\vec{F}_i^I =
\begin{bmatrix}
\rho u_i \\
\rho u_i u_1 + p \delta_{i1}\\
\rho u_i u_2 + p \delta_{i2}\\
\rho u_i u_3 + p \delta_{i3}\\ 
u_i (E + p) \\
\rho u_i Y_k
    \end{bmatrix},\qquad \vec{F}_i^V =
\begin{bmatrix}
0 \\
\tau_{1i}\\
\tau_{2i}\\
\tau_{3i}\\ 
u_j \tau_{ji} - q_i - \mathcal{Q}_i \\
-\mathcal{D}_i
    \end{bmatrix}
\end{equation}
for species $k=1,...,N_s-1$. The viscous stress tensor, heat conduction, species diffusion, and species heat flux are
\begin{align}
\tau_{ij} &= \mu \left( \frac{\partial u_i}{\partial x_j} + \frac{\partial u_j}{\partial x_i} - \frac{2}{3}\delta_{ij}\frac{\partial u_k}{\partial x_k} \right) + \mu_b \delta_{ij} \frac{\partial u_k}{\partial x_j}, \\
q_i &= -\kappa \frac{\partial T}{\partial x_i},\\
\mathcal{D}_i & = \underbrace{-\rho D_{m,\alpha} \frac{W_\alpha}{W} \frac{\partial \mathcal{X}_\alpha}{\partial x_i} - \rho Y_\alpha \sum_{\beta} D_{m,\beta} \frac{W_\beta}{W} \frac{\partial \mathcal{X}_\beta}{\partial x_j}}_{\textrm{need to check this with Popov slide and lit.}},\,\,\, \textrm{and}\\
\mathcal{Q}_i &= \underbrace{\sum_\beta \mathcal{D}_i\left( h_\alpha + \frac{R T \chi}{W_\alpha}\right)}_{\textrm{need to check this with Popov slide and lit.}} ,
\end{align}
\noindent respectively, where the shear viscosity ($\mu$), bulk viscosity ($\mu_b$), thermal conductivity ($\kappa$), thermal diffusion ratios ($\chi$), and binary diffusion coefficients ($D_{ij}$) (between the i$^\textrm{th}$ and j$^\textrm{th}$ species) are, in general, functions of thermodynamics state and are evaluated using Cantera library.  \textbf{[Does \textit{simplified transport} ignore bulk viscosity, Soret and Dufour effects?]}.   The energy assuming a thermally perfect gas mixture is
\begin{equation}
E = \frac{1}{2}\rho u_i u_i + \rho \sum_\alpha h_\alpha Y_\alpha -P,
\end{equation}
\noindent where the enthalpy is 
\begin{equation}
h_\alpha = \Delta h_\alpha^o + \int c_{p,\alpha}(T) dT
\end{equation}
\noindent and the functional form of the specific heats come from the NASA-7 polynomials.
The source term of enery and species density from chemical reactions takes
\begin{equation}
\vec{S} =
\underbrace{\begin{bmatrix}
0\\
0\\
0\\
0\\ 
\sum_i \Delta h^o_i\dot{\omega}_i \\
\dot{\omega}_k
    \end{bmatrix}}_{\textrm{we actually might be solving a diff enery from Y4}}.
\end{equation}

The viscous fluxes are discretized using repeated first-derivative, sixth-order centered difference with modified-third-order, one-sided stencils near boundaries to preserve a summation-by-parts property (which can facilitate a provable energy-stable boundary condition specification).  The inviscid fluxes are discretized using a fifth-order accurated weighted essentionally non-oscillatory (WENO) reconstruction of the invisicid flux characteristics since high-speed reactive flows have sharp spatial variations including for example shock waves.  WENO offers a nonlinear adaptive strategy for which high-order finite-differences are used for \textit{smooth} fields and upwinding-bias near \textit{non-smooth} fields (e.g. shocks) which depends on the local flow.  Near-boundary fluxes are approximated with a monotomic upwind scheme.  Time integration is performed with a standard fourth-order Runge--Kutta method.  

The complete description of the WENO is provided in full elsewhere.  Here, we explain the main features used in the current simulations.  Inviscid fluxes as in (\ref{eq:consv_eq}) are approximated by considering the conservative equation in the $i^{\textrm{th}}$
\begin{equation}
\frac{\partial}{\partial \xi_i}F(\vec{q}) \approx \frac{1}{\Delta_i}(\vec{f}_{i+1/2}-\vec{f}_{i-1/2})
\end{equation} 
where $\vec{f}$ is the numerical flux vector evaluated at midpoints around the $i^{\textrm{th}}$ position. For an $n^\textrm{th}$ order approximation to $f_{i+1/2}$ for example, $n$ points must be considered around the $i+1/2$ point.  This $n$-th order interpolated solution uses a set of candidate stencils. The WENO approach uses a combination of stencils by weighting each of them depending on the nature of the solution (e.g. smooth or non-smooth).  WENO reconstruction can be applied to any variable (i.e. conservative or primative), but choosing to do the reconstruction in the characteristic space can improve the robustness of the algorithm since the projection accounts for the direction of information propagation.  Characteristic decomposition of the inviscid flux vector for reacting, multicomponent flow is given in the following appendix.  Since there is a nonlinear temperature dependence of the thermodynamic variables these have additional terms in the energy inviscid Jacobian matrix.  


\section{Property-based testing}
\subsection{Equation of state properties}
\textbf{Purpose:} Test the correctness of analytical speed of sound\\
\textbf{Input:} $T_o$, $p_o$, $\rho_o$\\
\textbf{Output:} $\mathcal{E}(\delta)$\\
\textbf{Expected behavior:} The error $\mathcal{E}$ should reduce with $\delta$ density pertubration and thus have slope 1 (per first-order approxation) on a log-log ($\mathcal{E}$ vs. $\delta$) scale.  It should reach machine precision for small \textit{enough} $\delta$ and then be subject to machine round-off errors.\\ 
\textbf{Method:} Compute the analytical speed of sound ($c$) from the EOS interface and compare against its approximate value ($\tilde{c}$) based on a Taylor expansions from its thermodynamic definition assuming ($s-s_o=0$)
\begin{equation}
p = p_o + \underbrace{\frac{\partial p}{\partial \rho}\bigg{|}_{s_o}}_{c^2} (\rho-\rho_o) + O([\rho-\rho_o]^2) 
\end{equation}
thus the approximate square speed of sound is
\begin{equation}
\tilde{c}^2 = \frac{p-p_o}{\rho-\rho_o}.
\end{equation}
The relative error is calculated by specifying an incremental density pertubration $\delta$ above the ambient
\begin{equation}
\mathcal{E}(\delta) = \frac{|c^2-\tilde{c}^2|}{c^2} = \frac{|c^2-\frac{p-p_o}{\delta}|}{c^2}
\end{equation}
\noindent where the pressure due to density perturbation is calculated by
\begin{equation}
p = (\rho_o+\delta) R T_o
\end{equation}

\subsection{Jacobian of the Inviscid Fluxes}
\textbf{Purpose:} Generally test that the eigenvalue decomposition gives back the Jacobian of the inviscid flux\\
\vspace{6pt}\\
\textbf{Input:} $q_o(\vec{x}_i)$ and grid metrics (see restrictions below)\\
\vspace{6pt}\\
\textbf{Output:} $\mathcal{E}(\alpha)$ or (TRUE/FALSE) if error converges to first order in $\alpha$ and reaches machine precision\\
\vspace{6pt}\\
\textbf{Expected behavior:} The error $\mathcal{E}$ should reduce with size of perturbation ($\alpha$) and thus have slope 1 (per first-order approxation shown below) on a log-log ($\mathcal{E}$ vs. $\alpha$) scale.  It should reach machine precision for small \textit{enough} $\alpha$ and then be subject to machine round-off errors.  Based on testing gradients from discrete-exact adjoints, there will be a nonlinear region for large $\alpha$ for which the Taylor expansion does not hold and nonlinearity effects convergence.\\ 
\vspace{6pt}\\
\textbf{Method:} Compute the analytical inviscid flux jacobian from the matrix of right eigenvectors $R$ and the diagonal matrix of eigenvalues $\Lambda$ by
\begin{equation}
\frac{\partial E}{\partial q}\bigg{|}_{\vec{q}_o} = R \Lambda R^{-1},
\end{equation}
Taylor expanding the invisicd flux vector ($E$) for the $\vec{\xi}$-direction yields
\begin{equation}
E = E_o + \frac{\partial E}{\partial \vec{q}}\bigg{|}_{\vec{q}_o} (\vec{q}-\vec{q}_o) + O([\vec{q}-\vec{q}_o]^2) 
\label{eq:TaylorFlux}
\end{equation}
from a perturbation to the the base state ($\vec{q}_o$) by $\vec{q}=\vec{q}_o + \vec{\delta}$ where $\vec{\delta} = \alpha \left(R \Lambda R^{-1}\right)\vec{q}_o$ and $\alpha$ a real number. $E$ is evaluated at the perturbed state $\vec{q}$.  The choice of the perturbation direction ($\vec{\delta}$) is at our discretion and choosing $\vec{\delta}$ in the flux direction faciliates a description of the error as
\begin{equation}
\underbrace{\bigg{|}\bigg{|} \frac{E-E_o}{\alpha} - (R \Lambda R^{-1}) (R \Lambda R^{-1}) \vec{q}_o \bigg{|}\bigg{|}}_{\mathcal{E}} = \frac{1}{\alpha}O(\alpha^2[(R \Lambda R^{-1})\vec{q}_o (R \Lambda R^{-1})\vec{q}_o]).
\end{equation}
The error $\mathcal{E}$ decreases in proportion to $\alpha$ based on the right side. There should be no restrictions on $\vec{q}_o$ except that $\rho>0$, and $p=(\rho e - \frac{1}{2} \rho u_i u_i)(\gamma-1) > 0$ so that the speed of sound is real valued.   Any random metrics can also be tested $\vec{\xi}$ (though they may not represent a useful mapping).  This should be repeated for the inviscid fluxes in the $\vec{\eta}$, and $\vec{\zeta}$ directions.  Similar tests can be done for matrices of the left eigenvectors if those are also used.  For $\vec{q}$, I would however test several extreme conditions on $\vec{q}_o$
\begin{itemize}
\item Low inteneral energy, high kinetic energy 
\item Low kinetic energy, high internal energy
\item Non-zero velocities in all directions
\end{itemize}
%\begin{align}
%de &= T ds - P d\nu \\ 
%e-e_o & = T (s-s_o) - P (\nu - \nu_o)
%\end{align}
%\begin{align}
%c^2 &= \frac{\partial p}{\partial \rho}|_{ds=0} \approx \tilde{c}^2 \\
%\tilde{c}^2 &= \frac{p-p_o}{\rho-\rho_o}
%\end{align}

\subsection{Development of negative species near injector}
The conservation of mass and species (assuming no diffusion) for planar Cartesian coordinates is
\begin{align}
\frac{\partial \rho}{\partial t} &= -\frac{\partial \rho u}{\partial x} -\frac{\partial \rho v}{\partial y}\\
\frac{\partial \rho Y_i}{\partial t} &= -\frac{\partial \rho Y_i u}{\partial x} -\frac{\partial \rho Y_i v}{\partial y}
\end{align}
Using the chain rule, the species equation is expanded
\begin{equation}
\rho \frac{\partial Y_i}{\partial t}+Y_i\frac{\partial \rho}{\partial t}= -Y_i\frac{\partial \rho u}{\partial x} -Y_i\frac{\partial \rho v}{\partial y} - \rho u \frac{\partial Y_i}{\partial x} - \rho v \frac{\partial Y_i}{\partial y}
\end{equation}
For mass exactly conservered (discretely true?), the species conservation is
\begin{equation}
\frac{\partial Y_i}{\partial t} = -  u \frac{\partial Y_i}{\partial x} - v \frac{\partial Y_i}{\partial y}.
\end{equation}
The mass fraction on a no-slip wall should remain constant since ($u=v=0$ on the wall).  Sources of mass fraction changes, are thus 
\begin{itemize}
\item nonzero wall velocities
\item mass conservation not being discretely satisfied
\item or a mix of both.
\end{itemize}
Depending on the items above, the rate of change can be accentuated depending on the values of $\partial_x Y$ and $\partial_y Y$.  For a wall (with normal in the $y$-direction), no flux of species \textit{should} occur ($\partial_y Y|_{\textrm{wall}}=0$).  

To help prevent negative species, the following terms are proposed to add to the right-side of species 
\begin{equation}
S_i=-\sigma_{nn} (\rho Y_i) \qquad \textrm{for} Y_i < 0
\end{equation}
\noindent where $\sigma_{nn}=a \ln 10 / (N \Delta t)$ which would drive the negative mass fraction to a value of $10^{-a}$ of the negative value that triggered the source term over the time $N \Delta t$.

\section{Inputs}
\begin{table}[h!]
\begin{center}
\begin{tabular}{*{3}{l c c}} \hline
Flow ($\vec{x}_{LIB}$)   & &  \\\hline
$M$ &  &  \\
$T_o$ &  &  \\
%$u'_{\textrm{rms}}/v'_{\textrm{rms}}$ & 10. & 15. \\
$\overline{\phi}$ &  &  \\
$\phi'_{\textrm{rms}}$ &  &  \\
$\overline{T}$ (K) &  &  \\
$\overline{P}$ (kPa) &  & \\ 
$\delta_l/\Delta x$ &  &  \\
$t_s/t_k$ &  &  \\
$Ka=t_c/t_k$ &  & 
\\\hline
%\multicolumn{3}{c}{$(Re_r,Re_s)=(2\times10^5,10^3)$}
%\\\hline
    \end{tabular}
\end{center}
\caption{Flow configuration}
\end{table}

\begin{table}[h!]
\begin{center}
\begin{tabular}{*{3}{l c c}} \hline
Flow ($\vec{x}_{LIB}$)   & vertical injection & horizontal injection \\\hline
$\overline{M}$ &  &  \\
$u'_{\textrm{rms}}/\overline{U}$ &  &  \\
%$u'_{\textrm{rms}}/v'_{\textrm{rms}}$ & 10. & 15. \\
$\overline{\phi}$ &  &  \\
$\phi'_{\textrm{rms}}$ &  &  \\
$\overline{T}$ (K) &  &  \\
$\overline{P}$ (kPa) &  & \\ 
$\delta_l/\Delta x$ &  &  \\
$t_s/t_k$ &  &  \\
$Ka=t_c/t_k$ &  & 
\\\hline
%\multicolumn{3}{c}{$(Re_r,Re_s)=(2\times10^5,10^3)$}
%\\\hline
    \end{tabular}
\end{center}
\caption{Flow conditions at LIB breakdown position. $\delta_l$ is the laminar flame thickness at conditions $\overline{T}$, $\overline{P}$, $\overline{\phi}$. The turbulence time scale $t_k=\tilde{k}/\widetilde{\epsilon}$ (Pantano et al 2003). The scalar time scale $t_s=\widetilde{(Z'')^2}/\tilde{\chi}$ ($Z$ the mixture fraction) and $\chi$ the scalar dissipation rate proportional to $\overline{\partial Z'_k \partial Z'_k}$ (Pantano et al 2003). }
\end{table}

\pagebreak
\section{Wavenumber resolution of first derivative and filter}
The domain $(L_x,L_y)=(1,1)$ is discretized with $(N_x,N_y)=(129,129)$ uniformly spaced grid points.  The initial condition is
\begin{align}\label{eq:ic}
\rho(\vec{x}) &=1+ \epsilon \sum_{i=0}^{Nx/2+1} \sin\left(\frac{2 \pi i}{L_p} x + \phi_i\right)+\cos\left(\frac{2 \pi i}{L_p} x + \phi_i\right)\\\nonumber
\vec{u} &= [1, 0, 0]^{\mathrm{T}}
\end{align}
where $\epsilon=10^{-3}$, $L_p = L_x + \Delta=\Delta (N_x-1)+\Delta$, and $\phi_i$ is a pseudo random, mode-based phase between 0 and $2\pi$. At $i=N_x/2+1$, the smallest wavelength supported by the mesh is $2 \Delta$ (or $k\Delta=\pi$). The divergence of the mass flux, $(\rho u)_x$, based on the initial condition (\ref{eq:ic}), is evaluated numerically using different approaches to compare the effect of truncation error with other nonlinearly weighted schemes. The resulting power spectrum is shown in figure \ref{fig:resolutiondissipation} (a).  For the nonlinearly weighted scheme, it is also tested using a `highly dissipitive' form of the Lax splitting using the maximum eigenvalue of the characteristic waves (Nonomura \textit{et al.} 2015); this is denoted WENO$_D$.  The finite-difference schemes behave as expected (e.g. Lele 1992) however, the WENO schemes do not show a smooth behavior in the power spectrum.  Non-smoothness is expected from nonlinear weights and particular phasing of the initial condition, which was shown by Fauconnier \& Dick (2013).
\begin{figure}[h!]
\centering\includegraphics[page=2]{/Users/davidbuchta/Downloads/resolution-dissipation.pdf}
\caption{(a) First derivative: energy spectrum of momentum flux for standard centered and nonlinearly weighted finite-difference schemes. (b) Filter and dissipation: effect of artifical dissipation on normalized density spectrum for centered finite-difference schemes. }
\label{fig:resolutiondissipation}
\end{figure}

%\section{Effect of artificial dissipation}
Using the same initial condition as (\ref{eq:ic}) with $u=[0,0,0]^{\mathrm{T}}$, the inviscid flow equations are integrated in time to $t=L_x / c_\infty$ ($100 \Delta t$) and the density field compared to the initial condition since it should remain unchanged. The only contribution to changes in density thus come from the artificial dissipation since $\nabla \cdot \rho \mathbf{u}=0$ (since $\mathbf{u}=0$): for centered schemes this is a explicit source term and for nonlinear weighted scheme this is part of the algorithm itself. The density spectrum shown in figure \ref{fig:resolutiondissipation} (b).   Although higher accuracy schemes have larger dissipation beyond $k\Delta\approx 1$, which was anticipated from Mattsson et al. (2004), the dissipation for these schemes is delayed to higher wavenumbers.  That is, the wavenumber for $1\%$ dissipation compared to $\sigma=0$ is $k_{99}\Delta=0.098$ and $0.29$ for the second- and fourth-order schemes, respectively.  The WENO$_D$ scheme shows similar dissipation as the second-order centered approach but would need ensembled average to report its $k_{99}$.
%\begin{figure}[h!]
%\centering\includegraphics[page=1]{/Users/davidbuchta/Downloads/resolution-dissipation.pdf}
%\caption{Effect of artifical dissipation on density spectrum.}
%\end{figure}

\section{Free-stream preservation errors due to one-sided differences}
Still of course see this in 2D/3D; though I thought 2D was squashed or completely absent.

\section{Meshes}
\subsection{Nozzle to combustor with cavity}
For high-Reynolds number wall-bounded-flow simulations, an important mesh-design consideration is the near-wall mesh spacing $\Delta y^\star$ to resolve velocity gradient between no-slip walls and the free-stream.  For the current numerical scheme, previous convergence studies provide guidance that $\Delta y^\star \approx 0.25 y^+$ and $\Delta x \approx 20 y^+$ (private communication Bodony), which we aim to follow and confirm here. The $y^+$ is the standard wall unit length based on the wall sh	ear stress and viscosity. Since computational cost plays a role in its design, several initial meshes are tested in 2D as listed in table \ref{table:boundarymeshes}, which will be used to construct planes of the three-dimensional tunnel. The near-wall mesh stretching follows a geometric progression (Moxey et al \textit{Comput. Methods Appl. Mech. Engrg.} 2015)
\begin{equation}
\Delta y_i = \frac{\delta^\star (1-r) r^i}{1-r^{N}}:
\label{eq:meshStretch}
\end{equation}
\noindent $N$ is the number of points in the boundary layer mesh, $\delta^\star$ is the mesh width normal to the boundary, and $r$ modifies the stretching rate. For example, in the limit $r=1$, (\ref{eq:meshStretch}) uniform mesh-point spacing result for $[0,\delta^\star]$. The near-wall spacing is $\Delta y^\star=\Delta y(i=1)$ and $\delta^\star=3\textrm{mm}$.  \textcolor{red}{(Should $\delta^\star=A (x-x_i)+\delta^\star_o$?; relatively simple change, but not sure its necessary.)}

\begin{equation}
x_k = x_{k-1} + a r^k, a=\frac{2(1-r)}{1-r^{N+1}},
\end{equation}
\noindent where $\Delta_1=ar-1$  The modified version of Moxey set the near wall spacing and the width of the boundary layer mesh.  

\begin{table}[h!]
\begin{center}
\begin{tabular}{ c c c c | c}
\hline 
Description & $y^\star$ ($\mu$m) & $N_b$ & $r$ & $y^+(x=x_f)$ \\\hline
  0.5x & 32 & 24 & 1 & -- \\
  0.75x & 10.7 & 24 & 1 & -- \\
  1x & 16 & 32  & 32 & --\\ 
  1.5x & 6 & 48  & 1 & --\\ 
  2x & 8 & 64 & 1 & -- \\  
  4x & 4 & 128 & 1  & --\\ 
  8x & 2& 256 & 1  & --\\ \hline
\end{tabular} 
\caption{Mesh configurations with simulated boundary layer characteristics in the final column. Meshes 0.75x-4x are used for current mesh resolution study, while, [1, 2, 4, 8,16]x are used together for performance evalution. That is,  8x and 16x are not simulated to steady state themselves due to the computation expense and further time-step restriction.}
\label{table:boundarymeshes}
\end{center}
\end{table}  

%1x takes 79.947 seconds
%2x takes 317.749 seconds

%An expectation of the $y^+$ at the location of the cavity front wall can be written assuming $\delta=0$ at the nozzle throat, uniform velocity of $U=1035\mathrm{ms}^{-1}$.  Simulations indicate this value is of the correct order of magnitude for the simulated values.

\pagebreak
\subsection{Kolmogorov length scale}
The Kolmogorov length scale 
\begin{equation}
\eta = \left(\frac{\nu^3}{\epsilon}\right)^{1/4}
\end{equation}
decreases with dissipation rate 
\begin{equation}
\epsilon = 2 \nu \frac{\partial u_i}{\partial x_j}\frac{\partial u_i}{\partial x_j}.
\end{equation}
Dimensionally, the energy disspation rate is $[U^3L^{-1}]$ which comes from kinetic energy $U^2$ and rate $UL^{-1}$ (this is also the scaling for the turbulence production; that is, turbulence production equal to turbulence dissipation).  Take for example LIB-like characteristics $U=100\mathrm{m}/\mathrm{s}$, $L=1\mathrm{mm}$ and $\nu \approx 10^{-5}$; then $\eta=1\mu\mathrm{m}$ for $\epsilon=U^3/L$ or $\eta=8\mu\mathrm{m}$ for $\epsilon=2\nu U^2/L^2$.  Keep in mind the local velocity-gradient magnitude may be larger in space and time, especially during the early jet formation.
 
\subsection{Arc-heater to nozzle entrance}
\textcolor{red}{TODO: K. Mackay/Smith}

\subsection{Procedure outline for LIB integration}
\begin{enumerate}
\item Read in 8-grid HDF5 and 1-grid HDF5 into FORTRAN utility
\begin{itemize}
\item For 1-grid HDF5, change $\vec{x}$ to $\vec{x}'$ for the 1-grid HDF; $x'$ orgin is at breakdown location and rotated $2^\circ$ clockwise from vertical
\item Make three interpolation schemes
\begin{enumerate}
\item Grid 9 received all from grid 3: initialization period
\item Grid 9 receives from 3 and grid 3 receives from 9 with occlusion: LIB evolution period
\item Grid 3 received all from grid 9: removing LIB mesh after ionized species reduce
\end{enumerate}
\end{itemize}
\item Using first interpolation scheme, interpolate 8 grid solution onto the 9 grid domain 
\item Using second interpolation scheme, time integrate solution
\begin{itemize}
\item Evaluate solution correctness. Compare coarse background solution to this new local refined version.
\end{itemize}
\item Interpolate the MacArt solution onto new 9 grid solution and time integrate solution
\begin{itemize}
\item Pavel and MacArt have this mechanism in place but needs demonstrated on the 3D Y5 which we're doing here
\end{itemize}
\item After the LIB has developed and perhaps advected out of grid 9 (likely $> 25 \mu\mathrm{s}$, replace the interpolation weights with scheme 3.
\begin{itemize}
\item Take a time step so grid 9 interpolates to grid 3
\end{itemize}
\item Continue solution integration using original 8-grid interpolation weights; grid 9 no longer needed.
\end{enumerate}


%\subsection{Arc-heater to nozzle entrance}

\pagebreak
\subsection{`TORO3'}
\textbf{Purpose:} Assess accuracy of integrated WENO solver against the shock tube problem from E. Toro et al 2006.\\
\vspace{6pt}\\
\textbf{Input:} set $\vec{q}_l=$ and $\vec{q}_r=$ on a ($N_x$,$N_y$)=() uniform spaced mesh\\
\vspace{6pt}\\
\textbf{Output:} 1 or 0 based on relative error of $\vec{q}-\vec{q}_{a}$ where $\vec{q}_a$ is an accepted solution\\
\vspace{6pt}\\
\textbf{Expected behavior:} The relative error is expected to be machine precision.\\ 


\section{Meshes}
\subsection{Nozzle to combustor with cavity}
For high-Reynolds number wall-bounded-flow simulations, an important mesh-design consideration is the near-wall mesh spacing $\Delta y^\star$ to resolve velocity gradient between no-slip walls and the free-stream.  For the current numerical scheme, previous convergence studies provide guidance that $\Delta y^\star \approx 0.25 y^+$ and $\Delta x \approx 20 y^+$ (private communication Bodony), which we aim to follow and confirm here. The $y^+$ is the standard wall unit length based on the wall sh	ear stress and viscosity. Since computational cost plays a role in its design, several initial meshes are tested in 2D as listed in table \ref{table:boundarymeshes}, which will be used to construct planes of the three-dimensional tunnel. The near-wall mesh stretching follows a geometric progression (Moxey et al \textit{Comput. Methods Appl. Mech. Engrg.} 2015)
\begin{equation}
\Delta y_i = \frac{\delta^\star (1-r) r^i}{1-r^{N}}:
\label{eq:meshStretch}
\end{equation}
\noindent $N$ is the number of points in the boundary layer mesh, $\delta^\star$ is the mesh width normal to the boundary, and $r$ modifies the stretching rate. For example, in the limit $r=1$, (\ref{eq:meshStretch}) uniform mesh-point spacing result for $[0,\delta^\star]$. The near-wall spacing is $\Delta y^\star=\Delta y(i=1)$ and $\delta^\star=3\textrm{mm}$.  \textcolor{red}{(Should $\delta^\star=A (x-x_i)+\delta^\star_o$?; relatively simple change, but not sure its necessary.)}

\begin{equation}
x_k = x_{k-1} + a r^k, a=\frac{2(1-r)}{1-r^{N+1}},
\end{equation}
\noindent where $\Delta_1=ar-1$  The modified version of Moxey set the near wall spacing and the width of the boundary layer mesh.  

\begin{table}[h!]
\begin{center}
\begin{tabular}{ c c c c | c}
\hline 
Description & $y^\star$ ($\mu$m) & $N_b$ & $r$ & $y^+(x=x_f)$ \\\hline
  0.5x & 32 & 24 & 1 & -- \\
  0.75x & 10.7 & 24 & 1 & -- \\
  1x & 16 & 32  & 32 & --\\ 
  1.5x & 6 & 48  & 1 & --\\ 
  2x & 8 & 64 & 1 & -- \\  
  4x & 4 & 128 & 1  & --\\ 
  8x & 2& 256 & 1  & --\\ \hline
\end{tabular} 
\caption{Mesh configurations with simulated boundary layer characteristics in the final column. Meshes 0.75x-4x are used for current mesh resolution study, while, [1, 2, 4, 8,16]x are used together for performance evalution. That is,  8x and 16x are not simulated to steady state themselves due to the computation expense and further time-step restriction.}
\label{table:boundarymeshes}
\end{center}
\end{table}  

%1x takes 79.947 seconds
%2x takes 317.749 seconds

%An expectation of the $y^+$ at the location of the cavity front wall can be written assuming $\delta=0$ at the nozzle throat, uniform velocity of $U=1035\mathrm{ms}^{-1}$.  Simulations indicate this value is of the correct order of magnitude for the simulated values.

\pagebreak
\subsection{Kolmogorov length scale}
The Kolmogorov length scale 
\begin{equation}
\eta = \left(\frac{\nu^3}{\epsilon}\right)^{1/4}
\end{equation}
decreases with dissipation rate 
\begin{equation}
\epsilon = 2 \nu \frac{\partial u_i}{\partial x_j}\frac{\partial u_i}{\partial x_j}.
\end{equation}
Dimensionally, the energy disspation rate is $[U^3L^{-1}]$ which comes from kinetic energy $U^2$ and rate $UL^{-1}$ (this is also the scaling for the turbulence production; that is, turbulence production equal to turbulence dissipation).  Take for example LIB-like characteristics $U=100\mathrm{m}/\mathrm{s}$, $L=1\mathrm{mm}$ and $\nu \approx 10^{-5}$; then $\eta=1\mu\mathrm{m}$ for $\epsilon=U^3/L$ or $\eta=8\mu\mathrm{m}$ for $\epsilon=2\nu U^2/L^2$.  Keep in mind the local velocity-gradient magnitude may be larger in space and time, especially during the early jet formation.
 
%\subsection{Arc-heater to nozzle entrance}
%\textcolor{red}{TODO: K. Mackay/Smith}

\subsection{Procedure outline for LIB integration}
\begin{enumerate}
\item Read in 8-grid HDF5 and 1-grid HDF5 into FORTRAN utility
\begin{itemize}
\item For 1-grid HDF5, change $\vec{x}$ to $\vec{x}'$ for the 1-grid HDF; $x'$ orgin is at breakdown location and rotated $2^\circ$ clockwise from vertical
\item Make three interpolation schemes
\begin{enumerate}
\item Grid 9 received all from grid 3: initialization period
\item Grid 9 receives from 3 and grid 3 receives from 9 with occlusion: LIB evolution period
\item Grid 3 received all from grid 9: removing LIB mesh after ionized species reduce
\end{enumerate}
\end{itemize}
\item Using first interpolation scheme, interpolate 8 grid solution onto the 9 grid domain 
\item Using second interpolation scheme, time integrate solution
\begin{itemize}
\item Evaluate solution correctness. Compare coarse background solution to this new local refined version.
\end{itemize}
\item Interpolate the MacArt solution onto new 9 grid solution and time integrate solution
\begin{itemize}
\item Pavel and MacArt have this mechanism in place but needs demonstrated on the 3D Y5 which we're doing here
\end{itemize}
\item After the LIB has developed and perhaps advected out of grid 9 (likely $> 25 \mu\mathrm{s}$, replace the interpolation weights with scheme 3.
\begin{itemize}
\item Take a time step so grid 9 interpolates to grid 3
\end{itemize}
\item Continue solution integration using original 8-grid interpolation weights; grid 9 no longer needed.
\end{enumerate}



\pagebreak

\end{document}

